# Yet Another Social Network
Demo: http://socialnetwork.denispershin.com/  
Login: daenerys@targaryen.com  
Password: 123456

** Functionality: **

+ registration  
+ display profile  
+ edit profile  
+ upload profile picture  
+ authentication with Spring Security  
+ ajax loading of accounts and groups  
+ AJAX search autocomplete  
+ AJAX search with pagination   
+ upload and download avatar  
+ users export to XML  
+ messaging through WebSocket  
+ posting on wall  
+ groups creation  
+ adding/removing friends  

** Tools: **  
JDK 8, Spring 5, Spring Security, Spring Boot, JPA 2 / Hibernate 5 / Spring Data, XStream, jQuery 3, WebSocket, Bootstrap 4, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 2018.  

** Notes: **  
SQL tables is located in the `db/tables.sql`  
SQL ddl is located in the `db/ddl.sql`

** Screenshots **  
![](screenshots/1.png)  
![](screenshots/10.png)  
![](screenshots/2.png)
![](screenshots/3.png)
![](screenshots/4.png)
![](screenshots/5.png)
![](screenshots/6.png)
![](screenshots/11.png)
![](screenshots/7.png)
![](screenshots/8.png)
![](screenshots/9.png)

--  
**Pershin Denis**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)
