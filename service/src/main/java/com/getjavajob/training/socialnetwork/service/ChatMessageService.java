package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.ChatMessage;
import com.getjavajob.training.socialnetwork.dao.ChatMessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
public class ChatMessageService {

    @Autowired
    private ChatMessageDao chatMessageDao;

    public void save(ChatMessage chatMessage) {
        chatMessageDao.save(chatMessage);
    }

    public List<ChatMessage> getAllConversation(int senderId, int receiverId) {
        return chatMessageDao.getAllConversation(senderId, receiverId);
    }

    public Set<Account> getAllChats(int senderId) {
        List<ChatMessage> chatMessages = chatMessageDao.getAllChats(senderId);
        Set<Account> result = new LinkedHashSet<>();
        for (ChatMessage chatMessage : chatMessages) {
            if (chatMessage.getSender().getId() != senderId) {
                result.add(chatMessage.getSender());
            } else {
                result.add(chatMessage.getReceiver());
            }
        }
        return result;
    }
}