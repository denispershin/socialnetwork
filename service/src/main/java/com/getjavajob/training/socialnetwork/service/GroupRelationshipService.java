package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupRelationship;
import com.getjavajob.training.socialnetwork.common.GroupRelationshipPK;
import com.getjavajob.training.socialnetwork.dao.GroupRelationshipDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupRelationshipService {

    @Autowired
    private GroupRelationshipDao groupRelationshipDao;

    @Transactional
    public void insert(GroupRelationship groupRelationship) {
        groupRelationship.setId(new GroupRelationshipPK(groupRelationship.getUser().getId(), groupRelationship.getGroup().getId()));
        groupRelationshipDao.save(groupRelationship);
    }

    @Transactional
    public void update(GroupRelationship groupRelationship) {
        groupRelationship.setId(new GroupRelationshipPK(groupRelationship.getUser().getId(), groupRelationship.getGroup().getId()));
        groupRelationshipDao.save(groupRelationship);
    }

    @Transactional
    public void delete(GroupRelationship groupRelationship) {
        groupRelationship.setId(new GroupRelationshipPK(groupRelationship.getUser().getId(), groupRelationship.getGroup().getId()));
        groupRelationshipDao.delete(groupRelationship);
    }

    public GroupRelationship getGroupRelationshipStatus(int userId, int groupId) {
        return groupRelationshipDao.getRelationship(userId, groupId);
    }

    public List<Group> getAccountGroup(int accountId) {
        List<GroupRelationship> groupRelationshipList = groupRelationshipDao.findAllByUserId(accountId);
        List<Group> groups = new ArrayList<>();
        for (GroupRelationship groupRelationship : groupRelationshipList) {
            if (groupRelationship.getStatus() == 1) {
                groups.add(groupRelationship.getGroup());
            }
        }
        return groups;
    }

    public List<Group> getAccountOutcomingGroups(int accountId) {
        List<GroupRelationship> groupRelationships = groupRelationshipDao.findAllByUserId(accountId);
        List<Group> groups = new ArrayList<>();
        for (GroupRelationship groupRelationship : groupRelationships) {
            if (groupRelationship.getStatus() == 0) {
                groups.add(groupRelationship.getGroup());
            }
        }
        return groups;
    }

    public List<Account> getGroupMembers(Group group) {
        List<Account> accounts = new ArrayList<>();
        List<GroupRelationship> groupRelationships = groupRelationshipDao.findAllByGroupId(group.getId());
        for (GroupRelationship groupRelationship : groupRelationships) {
            if (groupRelationship.getStatus() == 1) {
                accounts.add(groupRelationship.getUser());
            }
        }
        return accounts;
    }

    public List<Account> getIncomingGroupMembers(Group group) {
        List<Account> accounts = new ArrayList<>();
        List<GroupRelationship> groupRelationships = groupRelationshipDao.findAllByGroupId(group.getId());
        for (GroupRelationship groupRelationship : groupRelationships) {
            if (groupRelationship.getStatus() == 0) {
                accounts.add(groupRelationship.getUser());
            }
        }
        return accounts;
    }
}