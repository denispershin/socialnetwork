package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.JmsNotificationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class JMSSender {

    private static final String MESSAGE_QUEUE = "message-queue";

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(JmsNotificationMessage message) {
        jmsTemplate.convertAndSend(MESSAGE_QUEUE, message);
    }
}