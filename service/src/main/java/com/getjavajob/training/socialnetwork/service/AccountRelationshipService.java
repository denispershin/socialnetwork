package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRelationship;
import com.getjavajob.training.socialnetwork.dao.AccountRelationshipDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountRelationshipService {

    @Autowired
    private AccountRelationshipDao relationshipDao;

    @Transactional
    public AccountRelationship insert(AccountRelationship relationship) {
        return relationshipDao.save(relationship);
    }

    @Transactional
    public AccountRelationship delete(AccountRelationship relationship) {
        relationshipDao.delete(relationship);
        return relationship;
    }

    @Transactional
    public AccountRelationship update(AccountRelationship relationship) {
        return relationshipDao.save(relationship);
    }

    public AccountRelationship getRelationshipStatus(int firstUser, int secondUser) {
        return relationshipDao.getRelationshipStatus(firstUser, secondUser);
    }

    public List<Account> getFriends(int accountId) {
        List<AccountRelationship> relationshipList = relationshipDao.findAllByUserOneIdOrUserTwoId(accountId);
        List<Account> friendList = new ArrayList<>();
        for (AccountRelationship relationship : relationshipList) {
            if (relationship.getStatus() == 1) {
                createRelationship(friendList, relationship, accountId);
            }
        }
        return friendList;
    }

    public List<Account> getIncomingRequest(int accountId) {
        List<AccountRelationship> relationshipList = relationshipDao.findAllByUserOneIdOrUserTwoId(accountId);
        List<Account> incomingRequest = new ArrayList<>();
        for (AccountRelationship relationship : relationshipList) {
            if (relationship.getStatus() == 0 && relationship.getActionUser().getId() != accountId) {
                createRelationship(incomingRequest, relationship, accountId);
            }
        }
        return incomingRequest;
    }

    public List<Account> getOutcomingRequest(int accountId) {
        List<AccountRelationship> relationshipList = relationshipDao.findAllByUserOneIdOrUserTwoId(accountId);
        List<Account> outcomingRequest = new ArrayList<>();
        for (AccountRelationship relationship : relationshipList) {
            if (relationship.getStatus() == 0 && relationship.getActionUser().getId() == accountId
                    || relationship.getStatus() == 2 && relationship.getActionUser().getId() != accountId) {
                createRelationship(outcomingRequest, relationship, accountId);
            }
        }
        return outcomingRequest;
    }

    private void createRelationship(List<Account> list, AccountRelationship relationship, int accountId) {
        if (relationship.getUserOne().getId() != accountId) {
            list.add(relationship.getUserOne());
        } else if (relationship.getUserTwo().getId() != accountId) {
            list.add(relationship.getUserTwo());
        }
    }
}