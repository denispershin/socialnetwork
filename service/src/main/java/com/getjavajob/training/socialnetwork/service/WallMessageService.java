package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.WallMessage;
import com.getjavajob.training.socialnetwork.dao.WallMessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WallMessageService {

    @Autowired
    private WallMessageDao wallMessageDao;

    public void save(WallMessage wallMessage) {
        wallMessageDao.save(wallMessage);
    }

    public void delete(int id) {
        wallMessageDao.deleteById(id);
    }

    public WallMessage findById(int id) {
        return wallMessageDao.findById(id);
    }

    public List<WallMessage> findByAccountReceiver(int id) {
        return wallMessageDao.findAllByAccountReceiverIdOrderByIdDesc(id);
    }

    public List<WallMessage> findByGroupReceiver(int id) {
        return wallMessageDao.findAllByGroupReceiverIdOrderByIdDesc(id);
    }
}