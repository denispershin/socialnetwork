package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Account getAccountById(int id) {
        return accountDao.findAccountById(id);
    }

    public List<Account> getAll() {
        return accountDao.findAll();
    }

    @Transactional
    public Account create(Account account) {
        account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
        account.setAccountRole(RoleType.USER);
        return accountDao.save(account);
    }

    @Transactional
    public Account update(Account account) {
        // if account updated by ADMIN, it should be equals.
        if (!account.getPassword().equals(accountDao.findAccountById(account.getId()).getPassword())) {
            account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
        }
        return accountDao.save(account);
    }

    @Transactional
    @Secured("ROLE_ADMIN")
    public void deleteById(int id) {
        accountDao.deleteById(id);
    }

    public Account getAccountByEmail(String email) {
        return accountDao.findAccountByEmail(email);
    }

    public List<Account> searchAccounts(String searchQuery) {
        return accountDao.findBySearchPattern(searchQuery);
    }

    public List<Account> searchAccountsPagination(String searchQuery, Pageable pageable) {
        return accountDao.findBySearchPatternPagination(searchQuery, pageable);
    }

    public boolean accountIsExist(String email) {
        return accountDao.findAccountByEmail(email) != null;
    }

    public List<String> getAdminAccountEmails() {
        return accountDao.findAccountsByAccountRoleAdmin();
    }
}