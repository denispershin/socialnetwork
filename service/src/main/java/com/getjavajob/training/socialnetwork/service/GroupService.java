package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupRelationship;
import com.getjavajob.training.socialnetwork.common.GroupRelationshipPK;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.dao.GroupDao;
import com.getjavajob.training.socialnetwork.dao.GroupRelationshipDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupService {

    @Autowired
    private GroupDao groupDao;
    @Autowired
    private GroupRelationshipDao groupRelationshipDao;

    public Group getGroupById(int id) {
        return groupDao.findGroupById(id);
    }

    public List<Group> getGroupsByCreatorId(int id) {
        return groupDao.findGroupByCreatorId(id);
    }

    @Transactional
    public Group create(Group group) {
        groupDao.save(group);
        GroupRelationship groupRelationship = new GroupRelationship();
        groupRelationship.setUser(group.getCreator());
        groupRelationship.setStatus(1);
        groupRelationship.setGroup(group);
        groupRelationship.setActionUser(group.getCreator());
        groupRelationship.setId(new GroupRelationshipPK(groupRelationship.getUser().getId(), groupRelationship.getGroup().getId()));
        groupRelationship.setGroupRole(RoleType.ADMIN);
        groupRelationshipDao.save(groupRelationship);
        return group;
    }

    @Transactional
    public Group update(Group group) {
        return groupDao.save(group);
    }

    @Transactional
    public void delete(Group group) {
        groupDao.delete(group);
    }

    public List<Group> searchGroup(String searchString) {
        return groupDao.findBySearchPattern(searchString);
    }

    public List<Group> searchGroupPagination(String searchQuery, Pageable pageable) {
        return groupDao.findBySearchPatternPagination(searchQuery, pageable);
    }
}