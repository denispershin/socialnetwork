package com.getjavajob.training.socialnetwork.service.test;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupRelationship;
import com.getjavajob.training.socialnetwork.common.GroupRelationshipPK;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.dao.GroupDao;
import com.getjavajob.training.socialnetwork.dao.GroupRelationshipDao;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {

    @Mock
    private GroupDao groupDao;
    @Mock
    private GroupRelationshipDao groupRelationshipDao;
    @InjectMocks
    private static GroupService groupService;
    private static Group group;

    @BeforeClass
    public static void fillDb() {
        group = new Group();
        group.setGroupName("First");
        Account account = new Account();
        account.setFirstName("Kolya");
        account.setLastName("Petrov");
        account.setMiddleName("Olegovich");
        account.setDob(of(1984, 10, 16));
        account.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        account.setEmail("kolya@petrov.ru");
        account.setPassword("123456");
        account.setSkype("kolyapetrov");
        account.setAccountRole(RoleType.USER);
        group.setCreator(account);
        GroupRelationship groupRelationship = new GroupRelationship();
        groupRelationship.setUser(group.getCreator());
        groupRelationship.setStatus(1);
        groupRelationship.setGroup(group);
        groupRelationship.setActionUser(group.getCreator());
        GroupRelationshipPK groupRelationshipPK = new GroupRelationshipPK();
        groupRelationshipPK.setUser(groupRelationship.getUser().getId());
        groupRelationshipPK.setGroup(groupRelationship.getGroup().getId());
        groupRelationship.setId(groupRelationshipPK);
        groupRelationship.setGroupRole(RoleType.ADMIN);
    }

    @Test
    public void create() {
        when(groupDao.save(group)).thenReturn(group);
        Group actual = groupService.create(group);
        assertEquals(group, actual);
    }

    @Test
    public void getGroupById() {
        when(groupDao.findGroupById(1)).thenReturn(group);
        assertEquals(groupService.getGroupById(1), group);
    }

    @Test
    public void getGroupByCreatorId() {
        List<Group> groups = new ArrayList<>(Arrays.asList(group));
        when(groupDao.findGroupByCreatorId(1)).thenReturn(groups);
        assertEquals(groupService.getGroupsByCreatorId(1), groups);
    }

    @Test
    public void delete() {
        groupService.delete(group);
        assertNull(groupService.getGroupById(1));
    }

    @Test
    public void update() {
        group.setGroupName("Second");
        when(groupDao.save(group)).thenReturn(group);
        Group actual = groupService.update(group);
        assertEquals(group, actual);
    }
}