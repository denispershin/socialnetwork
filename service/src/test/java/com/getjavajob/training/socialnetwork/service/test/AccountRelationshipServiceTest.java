package com.getjavajob.training.socialnetwork.service.test;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRelationship;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.AccountRelationshipDao;
import com.getjavajob.training.socialnetwork.service.AccountRelationshipService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountRelationshipServiceTest {

    @Mock
    private AccountRelationshipDao accountRelationshipDao;
    @Mock
    private AccountDao accountDao;
    @InjectMocks
    private AccountRelationshipService accountRelationshipService;

    private Account accountOne;
    private Account accountTwo;
    private AccountRelationship accountRelationship;

    @Before
    public void createAccountOne() {
        accountOne = new Account();
        accountOne.setFirstName("Kolya");
        accountOne.setLastName("Petrov");
        accountOne.setMiddleName("Olegovich");
        accountOne.setDob(of(1984, 10, 16));
        accountOne.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        accountOne.setEmail("kolya@petrov.ru");
        accountOne.setPassword("123456");
        accountOne.setSkype("kolyapetrov");
        accountOne.setAccountRole(RoleType.USER);
        accountDao.save(accountOne);
    }

    @Before
    public void createAccountTwo() {
        accountTwo = new Account();
        accountTwo.setFirstName("Olya");
        accountTwo.setLastName("Ivanova");
        accountTwo.setMiddleName("Ivanovna");
        accountTwo.setDob(of(1988, 1, 19));
        accountTwo.setHomeAddress("Moscow");
        accountTwo.setEmail("olya@ivanova.ru");
        accountTwo.setPassword("123456");
        accountTwo.setSkype("olyaivanova");
        accountTwo.setAccountRole(RoleType.USER);
        accountDao.save(accountTwo);
    }

    private void insertRelationshipForTest() {
        accountRelationship = new AccountRelationship();
        accountRelationship.setUserOne(accountOne);
        accountRelationship.setUserTwo(accountTwo);
        accountRelationship.setActionUser(accountOne);
        accountRelationship.setStatus(0);
        accountRelationshipDao.save(accountRelationship);
    }

    @Test
    public void insert() {
        when(accountRelationshipDao.save(accountRelationship)).thenReturn(accountRelationship);
        AccountRelationship actual = accountRelationshipService.insert(accountRelationship);
        assertEquals(accountRelationship, actual);
    }

    @Test
    public void delete() {
        accountRelationshipService.delete(accountRelationship);
        verify(accountRelationshipDao, times(1)).delete(accountRelationship);
    }

    @Test
    public void update() {
        when(accountRelationshipDao.save(accountRelationship)).thenReturn(accountRelationship);
        AccountRelationship actual = accountRelationshipService.insert(accountRelationship);
        assertEquals(accountRelationship, actual);
    }


}
