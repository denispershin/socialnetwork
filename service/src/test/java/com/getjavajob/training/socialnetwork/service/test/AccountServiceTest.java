package com.getjavajob.training.socialnetwork.service.test;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private static AccountDao accountDao;
    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @InjectMocks
    private static AccountService accountService;
    private static Account account;

    @Before
    public void init() {
        account = new Account();
        account.setFirstName("Kolya");
        account.setLastName("Petrov");
        account.setMiddleName("Olegovich");
        account.setDob(of(1984, 10, 16));
        account.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        account.setEmail("kolya@petrov.ru");
        account.setPassword("123456");
        account.setSkype("kolyapetrov");
        account.setAccountRole(RoleType.USER);
        account.setId(1);
    }

    @Test
    public void getAccountById() {
        when(accountDao.findAccountById(1)).thenReturn(account);
        Account actual = accountService.getAccountById(1);
        assertEquals(account, actual);
    }

    @Test
    public void getAccountByEmail() {
        when(accountDao.findAccountByEmail("kolya@petrov.ru")).thenReturn(account);
        Account actual = accountService.getAccountByEmail("kolya@petrov.ru");
        assertEquals(account, actual);
    }

    @Test
    public void getAll() {
        Account account2 = new Account();
        account2.setFirstName("Olya");
        account2.setLastName("Ivanova");
        account2.setMiddleName("Andreevna");
        account2.setDob(of(1986, 6, 8));
        account2.setHomeAddress("Moscow");
        account2.setEmail("olya@mail.ru");
        account2.setAccountRole(RoleType.USER);
        account2.setId(2);
        accountDao.save(account);
        accountDao.save(account2);
        List<Account> accountList = Arrays.asList(account, account2);
        when(accountService.getAll()).thenReturn(accountList);
        List<Account> actual = accountService.getAll();
        assertEquals(accountList, actual);
    }

    @Test
    public void create() {
        when(accountDao.save(account)).thenReturn(account);
        Account actual = accountService.create(account);
        assertEquals(account, actual);
    }

    @Test
    public void update() {
        account.setFirstName("Anna");
        when(accountDao.save(account)).thenReturn(account);
        when(accountDao.findAccountById(account.getId())).thenReturn(account);
        Account actual = accountService.update(account);
        assertEquals(account, actual);
    }

    @Test
    public void accountIsExist() {
        when(accountDao.findAccountByEmail("kolya@petrov.ru")).thenReturn(account);
        assertTrue(accountService.accountIsExist("kolya@petrov.ru"));
    }

    @Test
    public void searchAccounts() {
        List<Account> accounts = new ArrayList<>(Arrays.asList(account));
        when(accountDao.findBySearchPattern("Kolya")).thenReturn(accounts);
        List<Account> actual = accountService.searchAccounts("Kolya");
        assertEquals(accounts, actual);
    }
}