package com.getjavajob.training.socialnetwork.common;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "relationship")
@IdClass(AccountRelationshipPK.class)
public class AccountRelationship implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "userone")
    private Account userOne;
    @Id
    @ManyToOne
    @JoinColumn(name = "usertwo")
    private Account userTwo;
    private int status;
    @ManyToOne
    @JoinColumn(name = "actionuser")
    private Account actionUser;

    public AccountRelationship() {
    }

    public Account getUserOne() {
        return userOne;
    }

    public void setUserOne(Account userOne) {
        this.userOne = userOne;
    }

    public Account getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(Account userTwo) {
        this.userTwo = userTwo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Account getActionUser() {
        return actionUser;
    }

    public void setActionUser(Account actionUser) {
        this.actionUser = actionUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountRelationship that = (AccountRelationship) o;
        return status == that.status &&
                Objects.equals(userOne, that.userOne) &&
                Objects.equals(userTwo, that.userTwo) &&
                Objects.equals(actionUser, that.actionUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userOne, userTwo, status, actionUser);
    }
}