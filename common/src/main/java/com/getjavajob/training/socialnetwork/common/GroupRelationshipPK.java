package com.getjavajob.training.socialnetwork.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class GroupRelationshipPK implements Serializable {

    @Column(name = "userid")
    private Integer user;
    @Column(name = "groupid")
    private Integer group;

    public GroupRelationshipPK() {
    }

    public GroupRelationshipPK(Integer user, Integer group) {
        this.user = user;
        this.group = group;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupRelationshipPK that = (GroupRelationshipPK) o;
        return user.equals(that.user) &&
                group.equals(that.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, group);
    }
}
