package com.getjavajob.training.socialnetwork.common;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "wallmessage")
public class WallMessage {

    public enum TypeMessage {
        ACCOUNT, GROUP
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "sender")
    private Account sender;
    @ManyToOne
    @JoinColumn(name = "accountreceiver")
    private Account accountReceiver;
    @ManyToOne
    @JoinColumn(name = "groupreceiver")
    private Group groupReceiver;
    private String message;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate datesend;
    @Enumerated(EnumType.STRING)
    private TypeMessage typeMessage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public Account getAccountReceiver() {
        return accountReceiver;
    }

    public void setAccountReceiver(Account accountReceiver) {
        this.accountReceiver = accountReceiver;
    }

    public Group getGroupReceiver() {
        return groupReceiver;
    }

    public void setGroupReceiver(Group groupReceiver) {
        this.groupReceiver = groupReceiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDate getDatesend() {
        return datesend;
    }

    public void setDatesend(LocalDate datesend) {
        this.datesend = datesend;
    }

    public TypeMessage getTypeMessage() {
        return typeMessage;
    }

    public void setTypeMessage(TypeMessage typeMessage) {
        this.typeMessage = typeMessage;
    }
}