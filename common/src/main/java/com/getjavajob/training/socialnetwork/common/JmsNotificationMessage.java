package com.getjavajob.training.socialnetwork.common;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Component
public class JmsNotificationMessage implements Serializable {

    private String message;
    private List<String> emails;

    public JmsNotificationMessage() {
    }

    public JmsNotificationMessage(String message, List<String> emails) {
        this.message = message;
        this.emails = emails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

}