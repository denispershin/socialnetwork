package com.getjavajob.training.socialnetwork.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.persistence.Embeddable;
import javax.persistence.Table;

@Embeddable
@Table(name = "phones")
@XStreamAlias("phone")
public class Phone {

    private String phone;
    private String type;

    public Phone(String phone, String type) {
        this.phone = phone;
        this.type = type;
    }

    public Phone() {

    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}