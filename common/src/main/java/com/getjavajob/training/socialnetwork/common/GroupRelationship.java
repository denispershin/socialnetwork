package com.getjavajob.training.socialnetwork.common;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "grouprelationship")
public class GroupRelationship implements Serializable {

    @EmbeddedId
    private GroupRelationshipPK id;
    @MapsId("user")
    @ManyToOne
    @JoinColumn(name = "userid")
    private Account user;
    @MapsId("group")
    @ManyToOne
    @JoinColumn(name = "groupid")
    private Group group;
    private int status;
    @ManyToOne
    @JoinColumn(name = "actionuser")
    private Account actionUser;
    @Enumerated(value = EnumType.STRING)
    private RoleType groupRole;

    public GroupRelationship() {
    }

    public GroupRelationshipPK getId() {
        return id;
    }

    public void setId(GroupRelationshipPK id) {
        this.id = id;
    }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Account getActionUser() {
        return actionUser;
    }

    public void setActionUser(Account actionUser) {
        this.actionUser = actionUser;
    }

    public RoleType getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(RoleType groupRole) {
        this.groupRole = groupRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupRelationship that = (GroupRelationship) o;
        return status == that.status &&
                Objects.equals(id, that.id) &&
                Objects.equals(user, that.user) &&
                Objects.equals(group, that.group) &&
                Objects.equals(actionUser, that.actionUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, group, status, actionUser);
    }
}