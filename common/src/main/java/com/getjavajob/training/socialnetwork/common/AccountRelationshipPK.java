package com.getjavajob.training.socialnetwork.common;

import java.io.Serializable;
import java.util.Objects;

public class AccountRelationshipPK implements Serializable {

    private Integer userOne;
    private Integer userTwo;

    public AccountRelationshipPK() {
    }

    public Integer getUserOne() {
        return userOne;
    }

    public void setUserOne(Integer userOne) {
        this.userOne = userOne;
    }

    public Integer getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(Integer userTwo) {
        this.userTwo = userTwo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountRelationshipPK that = (AccountRelationshipPK) o;
        return userOne.equals(that.userOne) &&
                userTwo.equals(that.userTwo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userOne, userTwo);
    }
}
