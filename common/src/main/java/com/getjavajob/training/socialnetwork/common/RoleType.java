package com.getjavajob.training.socialnetwork.common;

import org.springframework.security.core.GrantedAuthority;

public enum RoleType implements GrantedAuthority {

    ADMIN, MODERATOR, USER;

    @Override
    public String getAuthority() {
        return "ROLE_" + name();
    }
}