package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatMessageDao extends JpaRepository<ChatMessage, Integer> {

    @Query("SELECT ch FROM ChatMessage ch WHERE (ch.sender.id = ?1 AND ch.receiver.id = ?2)" +
            " OR (ch.sender.id = ?2 AND ch.receiver.id = ?1) ORDER BY ch.id ASC")
    List<ChatMessage> getAllConversation(int senderId, int receiverId);

    @Query("SELECT DISTINCT ch FROM ChatMessage ch WHERE (ch.sender.id = ?1 OR ch.receiver.id = ?1)")
    List<ChatMessage> getAllChats(int senderId);

}