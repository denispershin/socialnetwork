package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Group;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupDao extends JpaRepository<Group, Integer> {

    Group findGroupById(Integer id);

    List<Group> findGroupByCreatorId(Integer id);

    @Query("SELECT g FROM Group g WHERE g.groupName LIKE %?1% OR g.groupDescription LIKE %?1%")
    List<Group> findBySearchPattern(String searchQuery);

    @Query("SELECT g FROM Group g WHERE g.groupName LIKE %?1% OR g.groupDescription LIKE %?1%")
    List<Group> findBySearchPatternPagination(String searchQuery, Pageable pageable);

}