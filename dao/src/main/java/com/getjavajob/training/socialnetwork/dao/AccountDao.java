package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.RoleType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountDao extends JpaRepository<Account, Integer> {

    @Query("SELECT a FROM Account a LEFT JOIN FETCH a.phone WHERE a.email = ?1")
    Account findAccountByEmail(String email);

    @Query("SELECT a FROM Account a LEFT JOIN FETCH a.phone WHERE a.id = ?1")
    Account findAccountById(int id);

    @Query("SELECT a FROM Account a WHERE a.firstName LIKE %?1% OR a.lastName LIKE %?1% OR a.middleName LIKE %?1% OR a.email LIKE %?1%")
    List<Account> findBySearchPattern(String searchQuery);

    @Query("SELECT a FROM Account a WHERE a.firstName LIKE %?1% OR a.lastName LIKE %?1% OR a.middleName LIKE %?1% OR a.email LIKE %?1%")
    List<Account> findBySearchPatternPagination(String searchQuery, Pageable pageable);

    @Query(value = "SELECT email FROM account WHERE accountrole = 'ADMIN'", nativeQuery = true)
    List<String> findAccountsByAccountRoleAdmin();

}