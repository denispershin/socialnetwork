package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.WallMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WallMessageDao extends JpaRepository<WallMessage, Integer> {

    WallMessage findById(int id);

    List<WallMessage> findAllByAccountReceiverIdOrderByIdDesc(int id);

    List<WallMessage> findAllByGroupReceiverIdOrderByIdDesc(int id);

}