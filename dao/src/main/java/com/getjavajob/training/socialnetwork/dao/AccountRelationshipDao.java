package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.AccountRelationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountRelationshipDao extends JpaRepository<AccountRelationship, Integer> {

    @Query("SELECT r FROM AccountRelationship r WHERE r.userOne.id = ?1 OR r.userTwo.id = ?1")
    List<AccountRelationship> findAllByUserOneIdOrUserTwoId(int id);

    @Query("SELECT r FROM AccountRelationship r WHERE r.userOne.id = ?1 AND r.userTwo.id = ?2")
    AccountRelationship getRelationshipStatus(int firstUser, int secondUser);

}