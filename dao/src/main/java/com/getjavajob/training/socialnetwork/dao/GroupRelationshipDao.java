package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.GroupRelationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GroupRelationshipDao extends JpaRepository<GroupRelationship, Integer> {

    List<GroupRelationship> findAllByUserId(int id);

    List<GroupRelationship> findAllByGroupId(int id);

    @Query("SELECT g FROM GroupRelationship g WHERE g.user.id = ?1 and g.group.id = ?2")
    GroupRelationship getRelationship(int userId, int groupId);

}