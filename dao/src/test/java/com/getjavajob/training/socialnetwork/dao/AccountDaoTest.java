package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Phone;
import com.getjavajob.training.socialnetwork.common.RoleType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-for-test.xml"})
@Transactional
public class AccountDaoTest {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AccountDao accountDao;
    private Account account;

    @Before
    public void createAccount() {
        account = new Account();
        account.setFirstName("Kolya");
        account.setLastName("Petrov");
        account.setMiddleName("Olegovich");
        account.setDob(of(1984, 10, 16));
        account.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        account.setEmail("kolya@petrov.ru");
        account.setPassword("123456");
        account.setSkype("kolyapetrov");
        account.setAccountRole(RoleType.USER);
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone("89216874134", "Home"));
        account.setPhone(phones);
    }

    @After
    public void clearDb() {
        String deleteFromAccount = "DELETE FROM ACCOUNT";
        String deleteFromPhone = "DELETE FROM PHONES";
        String restartId = "ALTER TABLE ACCOUNT ALTER COLUMN ID RESTART WITH 1";
        jdbcTemplate.execute(deleteFromAccount);
        jdbcTemplate.execute(deleteFromPhone);
        jdbcTemplate.execute(restartId);
    }

    @Test
    public void insert() {
        accountDao.save(account);
        List<Account> actual = new LinkedList<>();
        actual.add(account);
        List<Account> expected = accountDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void update() {
        accountDao.save(account);
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone("89211114488", "Home"));
        account.setPhone(phones);
        accountDao.save(account);
        List<Account> actual = new LinkedList<>();
        actual.add(account);
        List<Account> expected = accountDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void delete() {
        accountDao.save(account);
        Account accountId2 = new Account();
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone("89638743891", "Home"));
        accountId2.setFirstName("Petr");
        accountId2.setLastName("Ivanov");
        accountId2.setMiddleName("Nikolaevich");
        accountId2.setDob(of(1983, 4, 8));
        accountId2.setPhone(phones);
        accountId2.setHomeAddress("Moscow, Arbat str, d. 4, kv. 8");
        accountId2.setEmail("petr@ivanov.ru");
        accountId2.setPassword("123456");
        accountId2.setSkype("petrivanov");
        accountId2.setAccountRole(RoleType.USER);
        accountDao.save(accountId2);
        accountDao.delete(account);
        List<Account> actual = new LinkedList<>();
        actual.add(accountId2);
        List<Account> expected = accountDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void deleteById() {
        accountDao.save(account);
        Account accountId2 = new Account();
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone("89638743891", "Home"));
        accountId2.setFirstName("Petr");
        accountId2.setLastName("Ivanov");
        accountId2.setMiddleName("Nikolaevich");
        accountId2.setDob(of(1983, 4, 8));
        accountId2.setPhone(phones);
        accountId2.setHomeAddress("Moscow, Arbat str, d. 4, kv. 8");
        accountId2.setEmail("petr@ivanov.ru");
        accountId2.setPassword("123456");
        accountId2.setSkype("petrivanov");
        accountId2.setAccountRole(RoleType.USER);
        accountDao.save(accountId2);
        accountDao.deleteById(account.getId());
        List<Account> actual = new LinkedList<>();
        actual.add(accountId2);
        List<Account> expected = accountDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void getById() {
        accountDao.save(account);
        Optional<Account> getAccount = accountDao.findById(account.getId());
        assertEquals(account, getAccount.get());
    }

    @Test
    public void getAll() {
        accountDao.save(account);
        List<Account> actual = new LinkedList<>();
        actual.add(account);
        List<Account> expected = accountDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void accountIsExistTrue() {
        accountDao.save(account);
        assertEquals(account, accountDao.findAccountByEmail("kolya@petrov.ru"));
    }

    @Test
    public void accountIsExistFalse() {
        accountDao.save(account);
        assertNotEquals(account, accountDao.findAccountByEmail("anya@petrova.ru"));
    }

    @Test
    public void getByEmail() {
        accountDao.save(account);
        Account accountByEmail = accountDao.findAccountByEmail("kolya@petrov.ru");
        assertEquals(account, accountByEmail);
    }

    @Test
    public void getBySearchPattern() {
        accountDao.save(account);
        List<Account> accountDaoBySearchPattern = accountDao.findBySearchPattern("kolya@petrov.ru");
        assertEquals(account, accountDaoBySearchPattern.iterator().next());
    }
}