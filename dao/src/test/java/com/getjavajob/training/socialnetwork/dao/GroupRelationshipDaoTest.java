package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupRelationship;
import com.getjavajob.training.socialnetwork.common.GroupRelationshipPK;
import com.getjavajob.training.socialnetwork.common.RoleType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-for-test.xml"})
@Transactional
public class GroupRelationshipDaoTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private GroupRelationshipDao groupRelationshipDao;

    private Account account;
    private Account accountTwo;
    private Group groupOne;
    private Group groupSecond;
    private GroupRelationship insertedGroupRelationshipOne;
    private GroupRelationship insertedGroupRelationshipTwo;

    @Before
    public void fillDatabase() {
        createAccountOne();
        createAccountTwo();
        createGroupOne();
        createGroupTwo();
    }

    @After
    public void clearDatabase() {
        String deleteFromAccount = "DELETE FROM ACCOUNT";
        String restartId = "ALTER TABLE ACCOUNT ALTER COLUMN ID RESTART WITH 1";
        String deleteFromGroup = "DELETE FROM GROUPTABLE";
        String deleteFromGroupRelationship = "DELETE FROM GROUPRELATIONSHIP";
        jdbcTemplate.execute(deleteFromGroupRelationship);
        jdbcTemplate.execute(deleteFromGroup);
        jdbcTemplate.execute(deleteFromAccount);
        jdbcTemplate.execute(restartId);
    }

    @Test
    public void insert() {
        GroupRelationship expected = new GroupRelationship();
        GroupRelationshipPK groupRelationshipPK = new GroupRelationshipPK();
        groupRelationshipPK.setUser(account.getId());
        groupRelationshipPK.setGroup(groupOne.getId());
        expected.setId(groupRelationshipPK);
        expected.setGroup(groupOne);
        expected.setUser(account);
        expected.setGroupRole(RoleType.ADMIN);
        expected.setStatus(0);
        expected.setActionUser(account);
        groupRelationshipDao.save(expected);
        GroupRelationship actual = groupRelationshipDao.findAll().iterator().next();
        assertEquals(expected, actual);
    }

    @Test
    public void update() {
        insertGroupRelationshipForTestOne();
        GroupRelationship expected = groupRelationshipDao.findAll().iterator().next();
        expected.setStatus(1);
        groupRelationshipDao.save(expected);
        GroupRelationship actual = groupRelationshipDao.findAll().iterator().next();
        assertEquals(expected, actual);
    }

    @Test(expected = NoSuchElementException.class)
    public void delete() {
        insertGroupRelationshipForTestOne();
        groupRelationshipDao.delete(insertedGroupRelationshipOne);
        groupRelationshipDao.findAll().iterator().next();
    }

    @Test
    public void findAllByUserId() {
        insertGroupRelationshipForTestOne();
        insertGroupRelationshipForTestTwo();
        List<GroupRelationship> actual = groupRelationshipDao.findAllByUserId(account.getId());
        List<GroupRelationship> expected = new LinkedList<>(Collections.singletonList(insertedGroupRelationshipOne));
        assertEquals(expected, actual);
    }

    @Test
    public void findAllByGroupId() {
        insertGroupRelationshipForTestOne();
        List<GroupRelationship> actual = groupRelationshipDao.findAllByGroupId(groupOne.getId());
        List<GroupRelationship> expected = new LinkedList<>(Collections.singletonList(insertedGroupRelationshipOne));
        assertEquals(expected, actual);
    }

    @Test
    public void getRelationship() {
        insertGroupRelationshipForTestOne();
        insertGroupRelationshipForTestTwo();
        GroupRelationship actual = groupRelationshipDao.getRelationship(account.getId(), groupOne.getId());
        GroupRelationship expected = insertedGroupRelationshipOne;
        assertEquals(expected, actual);
    }

    private void createAccountOne() {
        account = new Account();
        account.setFirstName("Kolya");
        account.setLastName("Petrov");
        account.setMiddleName("Olegovich");
        account.setDob(of(1984, 10, 16));
        account.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        account.setEmail("kolya@petrov.ru");
        account.setPassword("123456");
        account.setSkype("kolyapetrov");
        account.setAccountRole(RoleType.USER);
        accountDao.save(account);
    }

    private void createAccountTwo() {
        accountTwo = new Account();
        accountTwo.setFirstName("Olya");
        accountTwo.setLastName("Ivanova");
        accountTwo.setMiddleName("Ivanovna");
        accountTwo.setDob(of(1988, 1, 19));
        accountTwo.setHomeAddress("Moscow");
        accountTwo.setEmail("olya@ivanova.ru");
        accountTwo.setPassword("123456");
        accountTwo.setSkype("olyaivanova");
        accountTwo.setAccountRole(RoleType.USER);
        accountDao.save(accountTwo);
    }

    private void createGroupOne() {
        groupOne = new Group();
        groupOne.setCreator(account);
        groupOne.setGroupName("Group name");
        groupOne.setGroupDescription("Group description");
        groupDao.save(groupOne);
    }

    private void createGroupTwo() {
        groupSecond = new Group();
        groupSecond.setCreator(account);
        groupSecond.setGroupName("Group name");
        groupSecond.setGroupDescription("Group description");
        groupDao.save(groupSecond);
    }

    private void insertGroupRelationshipForTestOne() {
        insertedGroupRelationshipOne = new GroupRelationship();
        GroupRelationshipPK groupRelationshipPK = new GroupRelationshipPK();
        groupRelationshipPK.setUser(account.getId());
        groupRelationshipPK.setGroup(groupOne.getId());
        insertedGroupRelationshipOne.setId(groupRelationshipPK);
        insertedGroupRelationshipOne.setGroup(groupOne);
        insertedGroupRelationshipOne.setUser(account);
        insertedGroupRelationshipOne.setGroupRole(RoleType.ADMIN);
        insertedGroupRelationshipOne.setStatus(0);
        insertedGroupRelationshipOne.setActionUser(account);
        groupRelationshipDao.save(insertedGroupRelationshipOne);
    }

    private void insertGroupRelationshipForTestTwo() {
        insertedGroupRelationshipTwo = new GroupRelationship();
        GroupRelationshipPK groupRelationshipPK = new GroupRelationshipPK();
        groupRelationshipPK.setUser(account.getId());
        groupRelationshipPK.setGroup(groupOne.getId());
        insertedGroupRelationshipTwo.setId(groupRelationshipPK);
        insertedGroupRelationshipTwo.setGroup(groupOne);
        insertedGroupRelationshipTwo.setUser(account);
        insertedGroupRelationshipTwo.setGroupRole(RoleType.ADMIN);
        insertedGroupRelationshipTwo.setStatus(0);
        insertedGroupRelationshipTwo.setActionUser(account);
        groupRelationshipDao.save(insertedGroupRelationshipTwo);
    }
}