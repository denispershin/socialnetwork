package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRelationship;
import com.getjavajob.training.socialnetwork.common.RoleType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-for-test.xml"})
@Transactional
public class AccountRelationshipDaoTest {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private AccountRelationshipDao accountRelationshipDao;
    private Account accountOne;
    private Account accountTwo;

    @Before
    public void createAccountOne() {
        accountOne = new Account();
        accountOne.setFirstName("Kolya");
        accountOne.setLastName("Petrov");
        accountOne.setMiddleName("Olegovich");
        accountOne.setDob(of(1984, 10, 16));
        accountOne.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        accountOne.setEmail("kolya@petrov.ru");
        accountOne.setPassword("123456");
        accountOne.setSkype("kolyapetrov");
        accountOne.setAccountRole(RoleType.USER);
        accountDao.save(accountOne);
    }

    @Before
    public void createAccountTwo() {
        accountTwo = new Account();
        accountTwo.setFirstName("Olya");
        accountTwo.setLastName("Ivanova");
        accountTwo.setMiddleName("Ivanovna");
        accountTwo.setDob(of(1988, 1, 19));
        accountTwo.setHomeAddress("Moscow");
        accountTwo.setEmail("olya@ivanova.ru");
        accountTwo.setPassword("123456");
        accountTwo.setSkype("olyaivanova");
        accountTwo.setAccountRole(RoleType.USER);
        accountDao.save(accountTwo);
    }

    private void insertRelationshipForTest() {
        AccountRelationship accountRelationship = new AccountRelationship();
        accountRelationship.setUserOne(accountOne);
        accountRelationship.setUserTwo(accountTwo);
        accountRelationship.setActionUser(accountOne);
        accountRelationship.setStatus(0);
        accountRelationshipDao.save(accountRelationship);
    }

    @After
    public void clearDb() {
        String truncate = "TRUNCATE TABLE RELATIONSHIP";
        entityManager.createNativeQuery(truncate);
    }

    @Test
    public void insert() {
        AccountRelationship accountRelationship = new AccountRelationship();
        accountRelationship.setUserOne(accountOne);
        accountRelationship.setUserTwo(accountTwo);
        accountRelationship.setActionUser(accountOne);
        accountRelationship.setStatus(0);
        accountRelationshipDao.save(accountRelationship);
        List<AccountRelationship> actual = new LinkedList<>();
        actual.add(accountRelationship);
        List<AccountRelationship> expected = accountRelationshipDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void update() {
        insertRelationshipForTest();
        AccountRelationship relationshipFromDb = accountRelationshipDao.findAll().iterator().next();
        relationshipFromDb.setStatus(1);
        relationshipFromDb.setActionUser(accountTwo);
        accountRelationshipDao.save(relationshipFromDb);
        List<AccountRelationship> actual = new LinkedList<>();
        actual.add(relationshipFromDb);
        List<AccountRelationship> expected = accountRelationshipDao.findAll();
        assertEquals(expected, actual);
    }

    @Test(expected = NoSuchElementException.class)
    public void delete() {
        insertRelationshipForTest();
        AccountRelationship relationshipFromDb = accountRelationshipDao.findAll().iterator().next();
        accountRelationshipDao.delete(relationshipFromDb);
        AccountRelationship actual = accountRelationshipDao.findAll().iterator().next();
    }

    @Test
    public void findAllByUserOneIdOrUserTwoId() {
        insertRelationshipForTest();
        List<AccountRelationship> expected = accountRelationshipDao.findAllByUserOneIdOrUserTwoId(accountOne.getId());
        AccountRelationship accountRelationship = new AccountRelationship();
        accountRelationship.setUserOne(accountOne);
        accountRelationship.setUserTwo(accountTwo);
        accountRelationship.setActionUser(accountOne);
        accountRelationship.setStatus(0);
        accountRelationshipDao.save(accountRelationship);
        List<AccountRelationship> actual = accountRelationshipDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void getRelationshipStatus() {
        insertRelationshipForTest();
        AccountRelationship accountRelationship = new AccountRelationship();
        accountRelationship.setUserOne(accountOne);
        accountRelationship.setUserTwo(accountTwo);
        accountRelationship.setActionUser(accountOne);
        accountRelationship.setStatus(0);
        AccountRelationship actual = accountRelationshipDao.getRelationshipStatus(accountOne.getId(), accountTwo.getId());
        AccountRelationship expected = accountRelationshipDao.findAll().iterator().next();
        assertEquals(expected, actual);
    }

}