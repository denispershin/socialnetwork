package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.Phone;
import com.getjavajob.training.socialnetwork.common.RoleType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-for-test.xml"})
@Transactional
public class GroupDaoTest {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private AccountDao accountDao;
    private Group group;
    private Account account;

    @Before
    public void fillDb() {
        account = new Account();
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone("89216874134", "Home"));
        account.setFirstName("Kolya");
        account.setLastName("Petrov");
        account.setMiddleName("Olegovich");
        account.setDob(of(1984, 10, 16));
        account.setPhone(phones);
        account.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        account.setEmail("kolya@petrov.ru");
        account.setPassword("123456");
        account.setSkype("kolyapetrov");
        account.setAccountRole(RoleType.USER);
        accountDao.save(account);
        group = new Group();
        group.setGroupName("First");
        group.setCreator(account);
    }

    @After
    public void clearDb() {
        String truncate = "TRUNCATE TABLE GROUPTABLE";
        String alter = "ALTER TABLE GROUPTABLE ALTER COLUMN ID RESTART WITH 1";
        entityManager.createNativeQuery(truncate);
        entityManager.createNativeQuery(alter);
    }

    @Test
    public void insert() {
        groupDao.save(group);
        List<Group> actual = new LinkedList<>();
        actual.add(group);
        List<Group> expected = groupDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void update() {
        groupDao.save(group);
        group.setGroupName("First Group");
        groupDao.save(group);
        List<Group> actual = new LinkedList<>();
        actual.add(group);
        List<Group> expected = groupDao.findAll();
        assertEquals(expected, actual);

    }

    @Test
    public void delete() {
        groupDao.save(group);
        Group groupId2 = new Group();
        groupId2.setGroupName("Second");
        groupId2.setCreator(account);
        groupDao.save(groupId2);
        groupDao.delete(group);
        List<Group> actual = new LinkedList<>();
        actual.add(groupId2);
        List<Group> expected = groupDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void deleteById() {
        groupDao.save(group);
        Group groupId2 = new Group();
        groupId2.setGroupName("Second");
        groupId2.setCreator(account);
        groupDao.save(groupId2);
        groupDao.deleteById(group.getId());
        List<Group> actual = new LinkedList<>();
        actual.add(groupId2);
        List<Group> expected = groupDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void getById() {
        groupDao.save(group);
        Group getGroup = groupDao.findGroupById(group.getId());
        assertEquals(group, getGroup);
    }

    @Test
    public void getAll() {
        groupDao.save(group);
        Group groupId2 = new Group();
        groupId2.setGroupName("Second");
        groupId2.setCreator(account);
        groupDao.save(groupId2);
        List<Group> actual = new LinkedList<>();
        actual.add(group);
        actual.add(groupId2);
        List<Group> expected = groupDao.findAll();
        assertEquals(expected, actual);
    }

    @Test
    public void getGroupsByCreatorId() {
        groupDao.save(group);
        List<Group> groups = groupDao.findAll();
        List<Group> groupsByCreator = groupDao.findGroupByCreatorId(account.getId());
        assertEquals(groups, groupsByCreator);
    }
}