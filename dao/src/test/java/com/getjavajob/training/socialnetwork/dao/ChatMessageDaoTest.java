package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.ChatMessage;
import com.getjavajob.training.socialnetwork.common.RoleType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-for-test.xml"})
@Transactional
public class ChatMessageDaoTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private ChatMessageDao chatMessageDao;

    private Account accountOne;
    private Account accountTwo;
    private ChatMessage chatMessageOne;
    private ChatMessage chatMessageTwo;

    @Before
    public void fillDatabase() {
        createAccountOne();
        createAccountTwo();
    }

    @After
    public void clearDatabase() {
        String deleteFromAccount = "DELETE FROM ACCOUNT";
        String restartAccountId = "ALTER TABLE ACCOUNT ALTER COLUMN ID RESTART WITH 1";
        String deleteFromChatMessages = "DELETE FROM CHATMESSAGES";
        String restartChatMessagesId = "ALTER TABLE CHATMESSAGES ALTER COLUMN ID RESTART WITH 1";
        jdbcTemplate.execute(deleteFromChatMessages);
        jdbcTemplate.execute(deleteFromAccount);
        jdbcTemplate.execute(restartAccountId);
        jdbcTemplate.execute(restartChatMessagesId);
    }

    @Test
    public void save() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setSender(accountOne);
        chatMessage.setReceiver(accountTwo);
        chatMessage.setContent("Привет");
        chatMessage.setDatetime(LocalDateTime.now());
        chatMessageDao.save(chatMessage);
        ChatMessage actual = chatMessageDao.getOne(chatMessage.getId());
        assertEquals(chatMessage, actual);
    }

    @Test
    public void getAllConversation() {
        insertCoversation();
        List<ChatMessage> expected = new LinkedList<>(Arrays.asList(chatMessageOne, chatMessageTwo));
        List<ChatMessage> actual = chatMessageDao.getAllConversation(accountOne.getId(), accountTwo.getId());
        assertEquals(expected, actual);
    }

    @Test
    public void getAllChats() {
        insertCoversation();
        List<ChatMessage> expected = new LinkedList<>(Arrays.asList(chatMessageOne, chatMessageTwo));
        List<ChatMessage> actual = chatMessageDao.getAllChats(accountOne.getId());
        assertEquals(expected, actual);
    }

    private void createAccountOne() {
        accountOne = new Account();
        accountOne.setFirstName("Kolya");
        accountOne.setLastName("Petrov");
        accountOne.setMiddleName("Olegovich");
        accountOne.setDob(of(1984, 10, 16));
        accountOne.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        accountOne.setEmail("kolya@petrov.ru");
        accountOne.setPassword("123456");
        accountOne.setSkype("kolyapetrov");
        accountOne.setAccountRole(RoleType.USER);
        accountDao.save(accountOne);
    }

    private void createAccountTwo() {
        accountTwo = new Account();
        accountTwo.setFirstName("Olya");
        accountTwo.setLastName("Ivanova");
        accountTwo.setMiddleName("Ivanovna");
        accountTwo.setDob(of(1988, 1, 19));
        accountTwo.setHomeAddress("Moscow");
        accountTwo.setEmail("olya@ivanova.ru");
        accountTwo.setPassword("123456");
        accountTwo.setSkype("olyaivanova");
        accountTwo.setAccountRole(RoleType.USER);
        accountDao.save(accountTwo);
    }

    private void insertCoversation() {
        chatMessageOne = new ChatMessage();
        chatMessageOne.setSender(accountOne);
        chatMessageOne.setReceiver(accountTwo);
        chatMessageOne.setContent("Привет");
        chatMessageOne.setDatetime(LocalDateTime.now());
        chatMessageDao.save(chatMessageOne);
        chatMessageTwo = new ChatMessage();
        chatMessageTwo.setSender(accountTwo);
        chatMessageTwo.setReceiver(accountOne);
        chatMessageTwo.setContent("Привет, как дела?");
        chatMessageTwo.setDatetime(LocalDateTime.now());
        chatMessageDao.save(chatMessageTwo);
    }
}