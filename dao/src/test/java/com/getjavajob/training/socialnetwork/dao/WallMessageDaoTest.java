package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.common.WallMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-for-test.xml"})
@Transactional
public class WallMessageDaoTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private WallMessageDao wallMessageDao;

    private Account accountOne;
    private Account accountTwo;
    private Group groupOne;

    @Before
    public void fillDatabase() {
        createAccountOne();
        createAccountTwo();
        createGroupOne();
    }

    @After
    public void clearDatabase() {
        String deleteFromAccount = "DELETE FROM ACCOUNT";
        String restartAccountId = "ALTER TABLE ACCOUNT ALTER COLUMN ID RESTART WITH 1";
        String deleteFromGroup = "DELETE FROM GROUPTABLE";
        String restartGroupId = "ALTER TABLE GROUPTABLE ALTER COLUMN ID RESTART WITH 1";
        String deleteFromWallmessage = "DELETE FROM WALLMESSAGE";
        jdbcTemplate.execute(deleteFromWallmessage);
        jdbcTemplate.execute(deleteFromGroup);
        jdbcTemplate.execute(deleteFromAccount);
        jdbcTemplate.execute(restartAccountId);
        jdbcTemplate.execute(restartGroupId);
    }

    @Test
    public void insertAccountWallMessage() {
        WallMessage wallMessage = new WallMessage();
        wallMessage.setSender(accountOne);
        wallMessage.setAccountReceiver(accountTwo);
        wallMessage.setMessage("hello");
        wallMessage.setDatesend(LocalDate.now());
        wallMessage.setTypeMessage(WallMessage.TypeMessage.ACCOUNT);
        wallMessageDao.save(wallMessage);
        WallMessage actual = wallMessageDao.findAll().iterator().next();
        assertEquals(wallMessage, actual);
    }

    @Test
    public void insertGroupWallMessage() {
        WallMessage wallMessage = new WallMessage();
        wallMessage.setSender(accountOne);
        wallMessage.setGroupReceiver(groupOne);
        wallMessage.setMessage("hello");
        wallMessage.setDatesend(LocalDate.now());
        wallMessage.setTypeMessage(WallMessage.TypeMessage.GROUP);
        wallMessageDao.save(wallMessage);
        WallMessage actual = wallMessageDao.findAll().iterator().next();
        assertEquals(wallMessage, actual);
    }

    @Test(expected = NoSuchElementException.class)
    public void delete() {
        WallMessage wallMessage = new WallMessage();
        wallMessage.setSender(accountOne);
        wallMessage.setAccountReceiver(accountTwo);
        wallMessage.setMessage("hello");
        wallMessage.setDatesend(LocalDate.now());
        wallMessage.setTypeMessage(WallMessage.TypeMessage.ACCOUNT);
        wallMessageDao.save(wallMessage);
        wallMessageDao.delete(wallMessage);
        wallMessageDao.findAll().iterator().next();
    }

    @Test
    public void findById() {
        WallMessage wallMessage = new WallMessage();
        wallMessage.setSender(accountOne);
        wallMessage.setAccountReceiver(accountTwo);
        wallMessage.setMessage("hello");
        wallMessage.setDatesend(LocalDate.now());
        wallMessage.setTypeMessage(WallMessage.TypeMessage.ACCOUNT);
        wallMessageDao.save(wallMessage);
        WallMessage actual = wallMessageDao.findById(wallMessage.getId());
        assertEquals(wallMessage, actual);
    }

    @Test
    public void findAllByAccountReceiverIdOrderByIdDesc() {
        WallMessage wallMessageOne = new WallMessage();
        wallMessageOne.setSender(accountOne);
        wallMessageOne.setAccountReceiver(accountTwo);
        wallMessageOne.setMessage("Привет");
        wallMessageOne.setDatesend(LocalDate.now());
        wallMessageOne.setTypeMessage(WallMessage.TypeMessage.ACCOUNT);
        wallMessageDao.save(wallMessageOne);
        WallMessage wallMessageTwo = new WallMessage();
        wallMessageTwo.setSender(accountOne);
        wallMessageTwo.setAccountReceiver(accountTwo);
        wallMessageTwo.setMessage("Как дела");
        wallMessageTwo.setDatesend(LocalDate.now());
        wallMessageTwo.setTypeMessage(WallMessage.TypeMessage.ACCOUNT);
        wallMessageDao.save(wallMessageTwo);
        List<WallMessage> expected = new LinkedList<>(Arrays.asList(wallMessageOne, wallMessageTwo));
        List<WallMessage> actual = wallMessageDao.findAllByAccountReceiverIdOrderByIdDesc(accountTwo.getId());
        actual.sort((o1, o2) -> {
            if (o1.getId() < o2.getId()) {
                return -1;
            } else if (o1.getId() > o2.getId()) {
                return 1;
            }
            return 0;
        });
        assertEquals(expected, actual);
    }

    @Test
    public void findAllByGroupReceiverIdOrderByIdDesc() {
        WallMessage wallMessageOne = new WallMessage();
        wallMessageOne.setSender(accountOne);
        wallMessageOne.setGroupReceiver(groupOne);
        wallMessageOne.setMessage("Стена группы");
        wallMessageOne.setDatesend(LocalDate.now());
        wallMessageOne.setTypeMessage(WallMessage.TypeMessage.GROUP);
        wallMessageDao.save(wallMessageOne);
        WallMessage wallMessageTwo = new WallMessage();
        wallMessageTwo.setSender(accountOne);
        wallMessageTwo.setGroupReceiver(groupOne);
        wallMessageTwo.setMessage("Привет всем.");
        wallMessageTwo.setDatesend(LocalDate.now());
        wallMessageTwo.setTypeMessage(WallMessage.TypeMessage.GROUP);
        wallMessageDao.save(wallMessageTwo);
        List<WallMessage> expected = new LinkedList<>(Arrays.asList(wallMessageOne, wallMessageTwo));
        List<WallMessage> actual = wallMessageDao.findAllByGroupReceiverIdOrderByIdDesc(groupOne.getId());
        actual.sort((o1, o2) -> {
            if (o1.getId() < o2.getId()) {
                return -1;
            } else if (o1.getId() > o2.getId()) {
                return 1;
            }
            return 0;
        });
        assertEquals(expected, actual);
    }

    private void createAccountOne() {
        accountOne = new Account();
        accountOne.setFirstName("Kolya");
        accountOne.setLastName("Petrov");
        accountOne.setMiddleName("Olegovich");
        accountOne.setDob(of(1984, 10, 16));
        accountOne.setHomeAddress("Saint-Petersburg, Nevskiy pr, d. 16, kv. 14");
        accountOne.setEmail("kolya@petrov.ru");
        accountOne.setPassword("123456");
        accountOne.setSkype("kolyapetrov");
        accountOne.setAccountRole(RoleType.USER);
        accountDao.save(accountOne);
    }

    private void createAccountTwo() {
        accountTwo = new Account();
        accountTwo.setFirstName("Olya");
        accountTwo.setLastName("Ivanova");
        accountTwo.setMiddleName("Ivanovna");
        accountTwo.setDob(of(1988, 1, 19));
        accountTwo.setHomeAddress("Moscow");
        accountTwo.setEmail("olya@ivanova.ru");
        accountTwo.setPassword("123456");
        accountTwo.setSkype("olyaivanova");
        accountTwo.setAccountRole(RoleType.USER);
        accountDao.save(accountTwo);
    }

    private void createGroupOne() {
        groupOne = new Group();
        groupOne.setCreator(accountOne);
        groupOne.setGroupName("Group name");
        groupOne.setGroupDescription("Group description");
        groupDao.save(groupOne);
    }
}