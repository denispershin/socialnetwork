CREATE TABLE IF NOT EXISTS `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `middlename` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `homeaddress` varchar(100) DEFAULT NULL,
  `workaddress` varchar(100) DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `icq` varchar(40) DEFAULT NULL,
  `skype` varchar(40) DEFAULT NULL,
  `additionalinformation` varchar(200) DEFAULT NULL,
  `registrationdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` mediumblob,
  `accountrole` varchar(10) NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_email_uindex` (`email`),
  UNIQUE KEY `account_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `phones` (
  `accountid` int(11) NOT NULL,
  `phone` varchar(12) DEFAULT NULL,
  `type` varchar(6) DEFAULT NULL,
  KEY `phones_account_id_fk` (`accountid`),
  CONSTRAINT `phones_account_id_fk` FOREIGN KEY (`accountid`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `grouptable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `creator` int(11) NOT NULL,
  `groupdescription` varchar(200) DEFAULT NULL,
  `registrationdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` mediumblob,
  PRIMARY KEY (`id`),
  KEY `grouptable_account_id_fk` (`creator`),
  CONSTRAINT `grouptable_account_id_fk` FOREIGN KEY (`creator`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `grouprelationship` (
  `userid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `actionuser` int(11) NOT NULL,
  `grouprole` varchar(10) NOT NULL,
  UNIQUE KEY `grouprelationship_pk` (`userid`,`groupid`),
  KEY `grouprelationship_account_id_fk_2` (`actionuser`),
  KEY `grouprelationship_grouptable_id_fk` (`groupid`),
  CONSTRAINT `grouprelationship_account_id_fk` FOREIGN KEY (`userid`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `grouprelationship_grouptable_id_fk` FOREIGN KEY (`groupid`) REFERENCES `grouptable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chatmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `content` mediumtext NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chatmessages_id_uindex` (`id`),
  KEY `chatmessages_account_id_fk` (`sender`),
  KEY `chatmessages_account_id_fk_2` (`receiver`),
  CONSTRAINT `chatmessages_account_id_fk` FOREIGN KEY (`sender`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chatmessages_account_id_fk_2` FOREIGN KEY (`receiver`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `relationship` (
  `userone` int(11) NOT NULL,
  `usertwo` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `actionuser` int(11) NOT NULL,
  PRIMARY KEY (`userone`,`usertwo`),
  KEY `relationship_account_id_fk` (`actionuser`),
  KEY `relationship_account_id_fk_3` (`usertwo`),
  CONSTRAINT `relationship_account_id_fk` FOREIGN KEY (`actionuser`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relationship_account_id_fk_2` FOREIGN KEY (`userone`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relationship_account_id_fk_3` FOREIGN KEY (`usertwo`) REFERENCES `account` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `wallmessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `accountreceiver` int(11) DEFAULT NULL,
  `groupreceiver` int(11) DEFAULT NULL,
  `message` varchar(255) NOT NULL,
  `datesend` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `typeMessage` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wallmessage_id_uindex` (`id`),
  KEY `wallmessage_account_id_fk` (`accountreceiver`),
  KEY `wallmessage_grouptable_id_fk` (`groupreceiver`),
  KEY `wallmessage_account_id_fk_2` (`sender`),
  CONSTRAINT `wallmessage_account_id_fk` FOREIGN KEY (`accountreceiver`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wallmessage_account_id_fk_2` FOREIGN KEY (`sender`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wallmessage_grouptable_id_fk` FOREIGN KEY (`groupreceiver`) REFERENCES `grouptable` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;