<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>

<div class="row grey lighten-5 no-gutters" style="min-height: 91vh;">
    <div class="col">
        <h5 class="mt-4 text-center" style="font-weight: 400">Friend list</h5>
        <c:if test="${requestScope.id == sessionScope.account.id}">
            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="friend-tab" data-toggle="tab" href="#friend" role="tab" aria-controls="friend" aria-selected="true">In friend (${fn:length(requestScope.friends)})</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="incoming-tab" data-toggle="tab" href="#incoming" role="tab" aria-controls="incoming" aria-selected="false">Incoming request (${fn:length(requestScope.incoming)})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="outcoming-tab" data-toggle="tab" href="#outcoming" role="tab" aria-controls="outcoming" aria-selected="false">Outcoming request (${fn:length(requestScope.outcoming)})</a>
                </li>
            </ul>
        </c:if>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="friend" role="tabpanel" aria-labelledby="friend-tab">
                <div class="row ml-auto mr-auto">
                    <c:forEach items="${requestScope.friends}" var="friend">
                        <div class="col-3 mt-3 text-center">
                            <a href="/account?id=${friend.id}"><h5>
                                <a href="/account?id=${friend.id}">${friend.firstName} ${friend.lastName}</a></h5></a>
                            <a href="/account?id=${friend.id}">
                                <img class="rounded mt-1 mb-2 z-depth-1" src="/image?id=${friend.id}" width="210" height="210"></a>
                                <c:if test="${requestScope.id == sessionScope.account.id}">
                                <a class="btn btn-secondary mt-2 btn-block" style="width: 210px" href="/conversation/${friend.id}" role="button"><em class="fas far fa-envelope-open pl-1 mr-2"></em>Send message</a>
                                <a class="btn btn-danger mt-2 btn-block" style="width: 210px" href="/friend?delete=${friend.id}" role="button"><em class="fas fa-user-minus pl-1 mr-2"></em>Remove friend</a>
                                </c:if>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <c:if test="${requestScope.id == sessionScope.account.id}">
                <div class="tab-pane fade" id="incoming" role="tabpanel" aria-labelledby="incoming-tab">
                    <div class="row ml-auto mr-auto">
                        <c:forEach items="${requestScope.incoming}" var="incoming">
                            <div class="col-3 mt-3 text-center">
                                <a href="/account?id=${friend.id}"><h5>
                                    <a href="/account?id=${incoming.id}">${incoming.firstName} ${incoming.lastName}</a>
                                </h5></a>
                                <a href="/account?id=${incoming.id}"><img class="rounded mt-1 mb-2 z-depth-1" src="/image?id=${incoming.id}" width="210" height="210"></a>
                                <a class="btn btn-success mt-2 btn-block" style="width: 210px" href="/friend?approve=${incoming.id}" role="button"><em class="fas fa-user-plus pl-1 mr-2"></em>Accept request</a>
                                <a class="btn btn-pink mt-2 btn-block" style="width: 210px" href="/friend?reject=${incoming.id}" role="button"><em class="fas fa-minus-circle pl-1 mr-2"></em>Reject request</a>
                                <a class="btn btn-danger mt-2 btn-block" style="width: 210px" href="/friend?delete=${incoming.id}" role="button"><em class="fas fa-user-minus pl-1 mr-2"></em>Remove request</a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <div class="tab-pane fade" id="outcoming" role="tabpanel" aria-labelledby="outcoming-tab">
                    <div class="row ml-auto mr-auto">
                        <c:forEach items="${requestScope.outcoming}" var="outcoming">
                            <div class="col-3 mt-3 text-center">
                                <a href="/account?id=${outcoming.id}"><h5>
                                    <a href="/account?id=${outcoming.id}">${outcoming.firstName} ${outcoming.lastName}</a>
                                </h5></a>
                                <a href="/account?id=${outcoming.id}"><img class="rounded mt-1 mb-2 z-depth-1" src="/image?id=${outcoming.id}" width="210" height="210"></a>
                                <a class="btn btn-danger mt-2 btn-block" style="width: 210px" href="/friend?delete=${outcoming.id}" role="button"><em class="fas fa-minus-circle pl-1 mr-2"></em>Delete request</a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>