<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters justify-content-between" style="min-height: 91vh;">
    <div class="col-8 mx-auto">
        <h5 class="mt-4 text-center" style="font-weight: 400">Create group</h5>
        <div class="col-11 border border-light rounded-lg pt-2 pb-3 mt-3">
            <form action="/doCreateGroup" method="post" enctype='multipart/form-data'>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><em class="fas fa-user"></em></span>
                    </div>
                    <input type="text" name="groupName" class="form-control" placeholder="Group name" aria-label="Group name" aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><em class="fas fa-info-circle"></em></span>
                    </div>
                    <textarea class="form-control rounded-0" name="groupDescription" id="exampleFormControlTextarea2" rows="3" placeholder="Group description" aria-label="Group description" aria-describedby="basic-addon1"></textarea>
                </div>
                <div class="input-default-wrapper mt-3">
                    <span class="input-group-text mb-3 border-right-0" style="border-bottom-right-radius: 0px; border-top-right-radius: 0px" id="input1"><em class="fas fa-camera-retro"></em></span>
                    <input type="file" name="avatar" id="file-with-current" class="input-default-js">
                    <label class="label-for-default-js rounded-right mb-3" for="file-with-current"><span class="span-choose-file">Choose group avatar</span>
                        <div class="float-right span-browse">Browse</div>
                    </label>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                        <button class="btn btn-info btn-block mt-3" type="submit">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>