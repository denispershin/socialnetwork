<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>

<div class="row grey lighten-5 no-gutters" style="min-height: 91vh;">
    <div class="col">
        <h5 class="mt-4 text-center" style="font-weight: 400">Member list of ${requestScope.group.groupName}</h5>
        <c:if test="${requestScope.group.creator.id == sessionScope.account.id
          or groupRelationshipStatus.groupRole == 'ADMIN'
          or groupRelationshipStatus.groupRole == 'MODERATOR'}">
            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="friend-tab" data-toggle="tab" href="#friend" role="tab"
                       aria-controls="friend"
                       aria-selected="true">In member list (${fn:length(requestScope.members)})</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="incoming-tab" data-toggle="tab" href="#incoming" role="tab"
                       aria-controls="incoming"
                       aria-selected="false">Incoming request (${fn:length(requestScope.incomingMembersList)})</a>
                </li>
            </ul>
        </c:if>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="friend" role="tabpanel" aria-labelledby="friend-tab">
                <div class="row ml-auto mr-auto">
                    <c:forEach items="${requestScope.members}" var="member">
                        <div class="col-3 mt-3 text-center">
                            <a href="#"><h5><a
                                    href="/account?id=${member.id}">${member.firstName} ${member.lastName}</a>
                            </h5></a>
                            <a href="#"><a href="/account?id=${member.id}"><img class="rounded mt-1 mb-2 z-depth-1"
                                                                                src="/image?id=${member.id}" width="210" height="210"></a>
                                <c:if test="${requestScope.group.creator.id == sessionScope.account.id
                                or groupRelationshipStatus.groupRole == 'ADMIN'
                                or groupRelationshipStatus.groupRole == 'MODERATOR'}">
                                <a class="btn btn-danger mt-2 btn-block" style="width: 210px"
                                   href="/grouprelationship?delete=${member.id}&groupid=${requestScope.group.id}" role="button"><em
                                        class="fas fa-user-minus pl-1 mr-2"></em>Delete member</a>
                                </c:if>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <c:if test="${requestScope.group.creator.id == sessionScope.account.id
            or groupRelationshipStatus.groupRole == 'ADMIN'
            or groupRelationshipStatus.groupRole == 'MODERATOR'}">
                <div class="tab-pane fade" id="incoming" role="tabpanel" aria-labelledby="incoming-tab">
                    <div class="row ml-auto mr-auto">
                        <c:forEach items="${requestScope.incomingMembersList}" var="incoming">
                            <div class="col-3 mt-3 text-center">
                                <a href="#"><h5><a
                                        href="/account?id=${incoming.id}">${incoming.firstName} ${incoming.lastName}</a>
                                </h5></a>
                                <a href="#"><img class="rounded mt-1 mb-2 z-depth-1" src="/image?id=${incoming.id}" width="210" height="210"></a>
                                <a class="btn btn-success mt-2 btn-block" style="width: 210px"
                                   href="/grouprelationship?approve=${incoming.id}&groupid=${requestScope.group.id}" role="button"><em
                                        class="fas fa-user-plus pl-1 mr-2"></em>Add to group</a>
                                <a class="btn btn-pink mt-2 btn-block" style="width: 210px"
                                   href="/grouprelationship?delete=${incoming.id}&groupid=${requestScope.group.id}" role="button"><em
                                        class="fas fa-minus-circle pl-1 mr-2"></em>Reject request</a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>