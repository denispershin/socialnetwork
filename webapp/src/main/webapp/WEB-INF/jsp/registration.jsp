<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Social Network</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/mdb.min.css" rel="stylesheet">
    <link href="/resources/css/style.css" rel="stylesheet">
    <link href="/resources/css/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery-3.4.0.js"></script>
    <script type="text/javascript" src="/resources/js/jquery-ui.js"></script>
    <style type="text/css">
        body, html {
            height: 100%;
        }

        .border-right {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .border-left {
            margin: -1px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

    </style>
</head>

<body class="mdb-color">
<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-6 bg-white no-gutters p-0 z-depth-1">
            <form class="text-center border border-light p-5" action="/doRegistration" method="post" enctype='multipart/form-data'>
                <p class="h4 mb-4">Sign up</p>
                <c:if test="${not empty requestScope.accountIsExist}">
                    <h4 class="card-title mt-3 text-center">${requestScope.accountIsExist}</h4>
                </c:if>
                <input type="text" name="firstName" class="form-control mb-3" placeholder="First name" required>
                <input type="text" name="lastName" class="form-control mb-3" placeholder="Last name" required>
                <input type="text" name="middleName" class="form-control mb-3" placeholder="Middle name">
                <input type="date" name="dob" class="form-control mb-3">
                <input type="email" name="email" class="form-control mb-3" placeholder="E-mail" required="">
                <input type="password" name="password" class="form-control mb-3" placeholder="Password" required="">
                <div id="inputPhoneHere"></div>
                <div class="input-group no-gutters mt-2">
                    <div class="col-2">
                        <select class="custom-select border-right" id="typeselect" name="type-no" style="border-top-right-radius: 0px; border-bottom-right-radius: 0px;">
                            <option value="Home" selected="">Home</option>
                            <option value="Mobile">Mobile</option>
                            <option value="Work">Work</option>
                        </select>
                    </div>
                    <input type="text" name="phone-no" class="form-control" placeholder="+79219999999">
                    <button type="button" onclick="func()" class="btn-sm btn-success px-3 border-0 border-left" id="btn-check-phone" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px">
                        <i class="fas fa-check"></i>
                    </button>
                </div>
                <div id="inputPhoneAlert"></div>
                <div class="input-default-wrapper mt-3">
                    <span class="input-group-text mb-3 border-right" id="input1">Upload</span>
                    <input type="file" name="avatar" id="file-with-current" class="input-default-js">
                    <label class="label-for-default-js rounded-right mb-3" for="file-with-current"><span class="span-choose-file">Choose avatar</span>
                        <div class="float-right span-browse">Browse</div>
                    </label>
                </div>

                <button class="btn btn-info my-4 btn-block" type="submit">Sign up</button>
                <p>Already registered?
                    <a href="/login">Login</a>
                </p>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript" src="/resources/js/phones.js"></script>
<script type="text/javascript" src="/resources/js/popper.min.js"></script>
<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/js/mdb.js"></script>
</body>
</html>