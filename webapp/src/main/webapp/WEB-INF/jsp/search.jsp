<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters" style="min-height: 91vh;">
    <input type="hidden" id="pattern" value="${search}">
    <input type="hidden" id="accountsSize" value="${accountsSize}">
    <input type="hidden" id="groupSize" value="${groupSize}">
    <c:choose>
        <c:when test="${not empty requestScope.blankQuery}">
            <h5 class="col text-center mt-3">${requestScope.blankQuery}</h5>
        </c:when>
        <c:otherwise>
            <div class="col"><h5 class="text-center mt-3">Accounts</h5>
                <c:if test="${not empty requestScope.accountsEmpty}">
                    <div class="col text-center">${requestScope.accountsEmpty}</div>
                </c:if>
                <div class="row justify-content-around" style="height: 250px">
                    <div class="col-1 ml-4 align-self-center">
                        <a id="backacc"><em class="fas fa-arrow-alt-circle-left fa-2x text-primary"></em></a></div>
                    <div class="col-9">
                        <div class="row justify-content-start" id="accountResult"></div>
                    </div>
                    <div class="col-1 ml-4 align-self-center">
                        <a id="nextacc"><em class="fas fa-arrow-alt-circle-right fa-2x text-primary"></em></a></div>
                </div>
                <div class="mt-5"></div>
                <hr>
                <h5 class="text-center mt-3">Groups</h5>
                <c:if test="${not empty requestScope.groupsEmpty}">
                    <div class="col text-center">${requestScope.groupsEmpty}</div>
                </c:if>
                <div class="row justify-content-around" style="height: 250px">
                    <div class="col-1 ml-4 align-self-center">
                        <a id="backgroup"><em class="fas fa-arrow-alt-circle-left fa-2x text-primary"></em></a></div>
                    <div class="col-9">
                        <div class="row justify-content-start" id="groupResult"></div>
                    </div>
                    <div class="col-1 ml-4 align-self-center">
                        <a id="nextgroup"><em class="fas fa-arrow-alt-circle-right fa-2x text-primary"></em></a></div>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<script type="text/javascript" src="/resources/js/searchPagination.js"></script>
<%@include file="footer.jsp" %>