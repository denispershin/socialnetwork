<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>

<div class="row grey lighten-5 no-gutters" style="min-height: 91vh;">
    <div class="col">
        <h5 class="mt-4 text-center" style="font-weight: 400">Group list</h5>
        <c:if test="${requestScope.id == sessionScope.account.id}">
            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="friend-tab" data-toggle="tab" href="#friend" role="tab" aria-controls="friend" aria-selected="true">In group list (${fn:length(requestScope.groups)})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="outcoming-tab" data-toggle="tab" href="#outcoming" role="tab" aria-controls="outcoming" aria-selected="false">Outcoming request (${fn:length(requestScope.outcoming)})</a>
                </li>
            </ul>
        </c:if>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="friend" role="tabpanel" aria-labelledby="friend-tab">
                <div class="row ml-auto mr-auto">
                    <c:forEach items="${requestScope.groups}" var="group">
                        <div class="col-3 mt-3 text-center">
                            <a href="#"><h5><a href="/group?id=${group.id}">${group.groupName}</a></h5>
                            </a><a href="#"><a href="/group?id=${group.id}">
                            <img class="rounded mt-1 mb-2 z-depth-1" src="/imagegroup/?groupid=${group.id}" width="210" height="210"></a>
                            <c:if test="${requestScope.id == sessionScope.account.id}">
                            <a class="btn btn-danger mt-2 btn-block" style="width: 210px" href="/grouprelationship?leave=${group.id}" role="button"><i class="fas fa-user-minus pl-1 mr-2"></i>Leave group</a>
                            </c:if>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <c:if test="${requestScope.id == sessionScope.account.id}">
                <div class="tab-pane fade" id="outcoming" role="tabpanel" aria-labelledby="outcoming-tab">
                    <div class="row ml-auto mr-auto">
                        <c:forEach items="${requestScope.outcoming}" var="outcoming">
                            <div class="col-3 mt-3 text-center">
                                <a href="#"><h5><a href="/group?id=${outcoming.id}">${outcoming.groupName}</a>
                                </h5></a>
                                <a href="#"><img class="rounded mt-1 mb-2 z-depth-1" src="/imagegroup/?groupid=${outcoming.id}" width="210" height="210"></a>
                                <a class="btn btn-danger mt-2 btn-block" style="width: 210px"
                                   href="/grouprelationship?leave=${outcoming.id}" role="button"><i class="fas fa-minus-circle pl-1 mr-2"></i>Delete request</a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>