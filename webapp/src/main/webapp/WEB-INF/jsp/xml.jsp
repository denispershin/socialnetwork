<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters justify-content-between" style="min-height: 91vh;">
    <div class="col-8 mx-auto">
        <h5 class="mt-4 text-center" style="font-weight: 400">Update account by XML </h5>
        <div class="col-12 border border-light rounded-lg pt-2 pb-3 mt-3">
            <form action="/exportxml" method="post" enctype='multipart/form-data'>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <p class="text-center mb-0">Save XML file with you data?</p>
                    </div>
                    <div class="col-4 text-center">
                        <button class="btn btn-success btn-block mt-3" type="submit">Download</button>
                    </div>

                </div>
            </form>
            <hr class="mt-4 mb-4">
            <form action="/xml" method="post" enctype='multipart/form-data'>
                <div class="col-12">
                    <p class="text-center">Do you want import you data in account from XML?</p>
                    <c:if test="${not empty existEmail}"><p class="text-center text-danger">${existEmail}</p></c:if>
                    <c:if test="${not empty notSameId}"><p class="text-center text-danger">${notSameId}</p></c:if>
                    <c:if test="${not empty notFile}"><p class="text-center text-danger">${notFile}</p></c:if>
                    <c:if test="${not empty wrongXml}"><p class="text-center text-danger">${wrongXml}</p></c:if>
                </div>
                <div class="input-default-wrapper mt-3">
                    <span class="input-group-text mb-3 border-right-0" style="border-bottom-right-radius: 0px; border-top-right-radius: 0px" id="input1"><em class="fas fa-camera-retro"></em></span>
                    <input type="file" name="xmlfile" id="file-with-current" class="input-default-js">
                    <label class="label-for-default-js rounded-right mb-3" for="file-with-current"><span class="span-choose-file">Choose XML file</span>
                        <div class="float-right span-browse">Browse</div>
                    </label>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                        <button class="btn btn-info btn-block mt-3" type="submit">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>