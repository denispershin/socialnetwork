<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters justify-content-between" style="min-height: 91vh;">
    <div class="col-3 mx-auto mt-5 text-center">
        <img class="rounded mt-3 z-depth-1" src="/imagegroup?groupid=${requestScope.group.id}" width="210" height="210">
        <c:if test="${requestScope.group.creator.id == sessionScope.account.id
          or groupRelationshipStatus.groupRole == 'ADMIN'
          or groupRelationshipStatus.groupRole == 'MODERATOR'}">
            <a class="btn btn-info mt-2 btn-block" style="width: 210px" href="/updategroup?id=${requestScope.group.id}"
               role="button"><em class="fas fa-user-edit pl-1 mr-2"></em>Update group</a>
        </c:if>
        <c:if test="${requestScope.group.creator.id != sessionScope.account.id}">
            <c:choose>
                <c:when test="${requestScope.groupRelationshipStatus.status == 1}">
                    <button type="button" class="btn btn-success mt-2 btn-block" style="width: 210px" disabled>
                        Already in group
                    </button>
                    <a class="btn btn-danger mt-2 btn-block" style="width: 210px"
                       href="/grouprelationship?leave=${requestScope.group.id}" role="button" aria-disabled="true"><em
                            class="fas fa-user-plus pl-1 mr-2"></em>Leave group</a>
                </c:when>
                <c:when test="${requestScope.groupRelationshipStatus.status == 0}">
                    <button type="button" class="btn btn-success mt-2 btn-block" style="width: 210px" disabled><em
                            class="fas fa-user-plus pl-1 mr-2"></em>Request is send
                    </button>
                    <a class="btn btn-danger mt-2 btn-block" style="width: 210px"
                       href="/grouprelationship?leave=${requestScope.group.id}" role="button"><em
                            class="fas fa-user-plus pl-1 mr-2"></em>Delete request</a>

                </c:when>
                <c:otherwise>
                    <a class="btn btn-success mt-2 btn-block" style="width: 210px"
                       href="/grouprelationship?sendrequest=${requestScope.group.id}" role="button"><em
                            class="fas fa-user-plus pl-1 mr-2"></em>Join to group</a>
                </c:otherwise>
            </c:choose>
        </c:if>
    </div>

    <div class="col-8 mx-auto">
        <h5 class="mt-4 text-center" style="font-weight: 400">Information</h5>
        <c:if test="${not empty updateerror}">${updateerror}</c:if>
        <div class="col-11 border border-light rounded-lg pt-2 pb-3 mt-3">
            <div class="col mt-2">Group name: <span class="font-parameter">${requestScope.group.groupName}</span></div>
            <div class="col mt-2">Description: <span
                    class="font-parameter">${requestScope.group.groupDescription}</span></div>
            <div class="col mt-2">Group creator: <a
                    href="/account?id=${requestScope.account.id}">${requestScope.account.firstName} ${requestScope.account.lastName}</a>
            </div>
            <div class="col mt-2">Registration Date: <span
                    class="font-parameter">${requestScope.group.registrationDate}</span></div>
            <hr>
            <h5 class="text-center"><a href="/memberlist?id=${requestScope.group.id}">Members
                (${fn:length(memberList)})</a></h5>
            <div class="row ml-3 mr-3 no-gutters">
                <c:forEach items="${requestScope.memberList}" var="member" begin="0" end="5">
                    <div class="col-2 text-center">
                        <a href="/account?id=${member.id}">
                            <img class="rounded-circle mt-2 mb-2 z-depth-1 center-block" src="/image?id=${member.id}"
                                 width="60" height="60">
                        </a>
                        <div class="text-center">
                            <a href="/account?id=${member.id}" style="font-size: 15px">${member.firstName}</a></div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <h5 class="mt-4 text-center" style="font-weight: 400">Wall message</h5>
        <div class="col-11 mt-1 p-0">
            <form action="/sendtogroup" method="post">
                <div class="form-group green-border-focus mb-1 mt-1">
                    <input name="groupReceiver" value="${group.id}" hidden>
                    <textarea class="form-control" rows="3" placeholder="Enter your message" name="message"></textarea>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary btn-sm">send</button>
                </div>
            </form>
        </div>
        <div class="col-11 mt-2">
            <c:forEach items="${wallMessages}" var="wallMessage">
                <div class="row mt-2 border border-light rounded-lg white">
                    <div class="col-1 text-center">
                        <a href="/account?id=${wallMessage.sender.id}"><img class="rounded-circle z-depth-1 center-block mt-2" src="/image?id=${wallMessage.sender.id}" width="60" height="60"></a>
                    </div>
                    <div class="col-10 ml-3 ">
                        <div class="row">
                            <div class="col-12 font-weight-bolder mt-1 text-primary">
                                <a href="/account?id=${wallMessage.sender.id}">${wallMessage.sender.firstName} ${wallMessage.sender.lastName}</a>
                            </div>
                            <div class="col-12 mt-1">${wallMessage.message}</div>
                            <div class="col-10 font-weight-light mt-1 mb-1 d-flex justify-content-end" style="font-size: 11px;">${wallMessage.datesend}</div>
                            <c:if test="${account.id == sessionScope.account.id || wallMessage.sender.id == sessionScope.account.id}">
                                <div class="col-1 font-weight-light mt-1 mb-1 d-flex justify-content-end" style="font-size: 11px;">
                                    <a href="/deletegroupmessage?id=${wallMessage.id}">Delete</a></div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>