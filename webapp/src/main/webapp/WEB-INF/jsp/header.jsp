<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Social Network</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="/resources/css/bootstrap.css" rel="stylesheet">
    <link href="/resources/css/mdb.css" rel="stylesheet">
    <link href="/resources/css/style.css" rel="stylesheet">
    <link href="/resources/css/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery-3.4.0.js"></script>
    <script type="text/javascript" src="/resources/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/resources/js/search.js"></script>

</head>

<body class="mdb-color">
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark blue darken-1">
        <a class="navbar-brand" href="/">Social Network</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="basicExampleNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/account?id=${sessionScope.account.id}">My page</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/friendlist?id=${sessionScope.account.id}">Friends</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/grouplist?id=${sessionScope.account.id}">Groups</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/chatlist">Messages</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/allaccounts">All accounts</a>
                </li>
            </ul>
            <form class="form-inline" action="/search" method="post">
                <div class="md-form my-0 ui-widget">
                    <input class="form-control mr-sm-2" name="search" id="ajaxsearch" type="text" placeholder="Search" aria-label="Search">
                    <a class="btn pink darken-1 btn-md my-2 my-sm-0 ml-3" href="/logout" role="button">Logout</a>
                </div>
            </form>
        </div>
    </nav>