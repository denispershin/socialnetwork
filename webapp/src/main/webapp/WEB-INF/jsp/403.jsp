<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters justify-content-between" style="min-height: 91vh;">
    <div class="col-8 mx-auto">
        <h1 class="mt-4 text-center" style="font-weight: 400">Access denied</h1>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>