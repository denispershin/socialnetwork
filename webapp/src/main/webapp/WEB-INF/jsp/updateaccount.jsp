<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters justify-content-between" style="min-height: 91vh;">
    <div class="col-8 mx-auto">

        <h5 class="mt-4 text-center" style="font-weight: 400">Update account</h5>
        <c:if test="${not empty requestScope.emailIsExist}">${requestScope.emailIsExist}
        </c:if>
        <div class="col-11 border border-light rounded-lg pt-2 pb-3 mt-3">
            <form action="/doUpdateAccount" id="formUpdate" method="post" enctype='multipart/form-data'>
                <input name="id" value="${updatedAccount.id}" hidden>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-user"></em></span>
                    </div>
                    <input type="text" name="firstName" class="form-control" value="${updatedAccount.firstName}" placeholder="First name">
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-user"></em></span>
                    </div>
                    <input type="text" name="lastName" class="form-control" value="${updatedAccount.lastName}" placeholder="Last name">
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-user"></em></span>
                    </div>
                    <input type="text" name="middleName" class="form-control" value="${updatedAccount.middleName}" placeholder="Middle name">
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-calendar-day"></em></span>
                    </div>
                    <input type="date" name="dob" class="form-control" value="${updatedAccount.dob}" placeholder="Date of birth">
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-at"></em></span>
                    </div>
                    <input type="text" name="email" class="form-control" value="${updatedAccount.email}" placeholder="E-mail">
                </div>
                <c:if test="${updatedAccount.id == sessionScope.account.id}">
                    <div class="input-group mb-3 mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><em class="fas fa-key"></em></span>
                        </div>
                        <input type="password" name="password" class="form-control" value="" placeholder="Password" required>
                    </div>
                </c:if>
                <c:forEach items="${updatedAccount.phone}" var="phone" varStatus="i">
                    <div class="input-group no-gutters mt-3 mb-3" id="removePhoneHere">
                        <div class="col-2">
                            <select class="custom-select border-right" name="phone[${i.index}].type" style="border-top-right-radius: 0px; border-bottom-right-radius: 0px;">
                                <option value="Home" ${phone.type == 'Home' ? 'selected' : ''}>Home</option>
                                <option value="Mobile" ${phone.type == 'Mobile' ? 'selected' : ''}>Mobile</option>
                                <option value="Work" ${phone.type == 'Work' ? 'selected' : ''}>Work</option>
                            </select>
                        </div>
                        <input type="text" name="phone[${i.index}].phone" class="form-control" value="${phone.phone}" readonly>
                        <button type="button" class="btn-sm btn-danger px-3 border-0 border-left" id="btn-remove-phone" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px">
                            <em class="fas fa-times fa-lg"></em></button>
                    </div>
                </c:forEach>
                <div id="inputPhoneHere"></div>
                <div class="input-group no-gutters mt-2">
                    <div class="col-2">
                        <select class="custom-select border-right" id="typeselect" name="type-no" style="border-top-right-radius: 0px; border-bottom-right-radius: 0px;">
                            <option value="Home" selected="">Home</option>
                            <option value="Mobile">Mobile</option>
                            <option value="Work">Work</option>
                        </select>
                    </div>
                    <input type="text" name="phone-no" class="form-control" placeholder="+79219999999">
                    <button type="button" onclick="func()" class="btn-sm btn-success px-3 border-0 border-left" id="btn-check-phone" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px">
                        <em class="fas fa-check"></em>
                    </button>
                </div>
                <div id="inputPhoneAlert"></div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><em class="fas fa-home"></em></span>
                    </div>
                    <textarea class="form-control rounded-0" name="homeAddress" rows="3" placeholder="Home address" aria-label="Home address">${updatedAccount.homeAddress}</textarea>
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-building"></em></span>
                    </div>
                    <textarea class="form-control rounded-0" name="workAddress" rows="3" placeholder="Work address" aria-label="Work address">${updatedAccount.workAddress}</textarea>
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-comment-dots"></em></span>
                    </div>
                    <input type="text" class="form-control" name="icq" placeholder="ICQ" value="${updatedAccount.icq}">
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fab fa-skype"></em></span>
                    </div>
                    <input type="text" class="form-control" name="skype" placeholder="Skype" value="${updatedAccount.skype}">
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-question-circle"></em></span>
                    </div>
                    <textarea class="form-control rounded-0" name="additionalInformation" rows="3" placeholder="Additional information">${updatedAccount.additionalInformation}</textarea>
                </div>
                <c:if test="${sessionScope.account.accountRole == 'ADMIN'}">
                    <div class="input-group mb-3 mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><em class="fas fa-question-circle"></em></span>
                        </div>
                        <select class="browser-default custom-select" name="accountRole">
                            <option value="ADMIN" ${updatedAccount.accountRole == 'ADMIN' ? 'selected' : ''}>ADMIN</option>
                            <option value="USER" ${updatedAccount.accountRole == 'USER' ? 'selected' : ''}>USER</option>
                        </select>
                    </div>
                </c:if>
                <div class="input-default-wrapper mt-3">
                    <span class="input-group-text mb-3 border-right-0" style="border-bottom-right-radius: 0px; border-top-right-radius: 0px" id="input1"><em class="fas fa-camera-retro"></em></span>
                    <input type="file" name="avatar" id="file-with-current" class="input-default-js">
                    <label class="label-for-default-js rounded-right mb-3" for="file-with-current"><span class="span-choose-file">Choose you avatar</span>
                        <div class="float-right span-browse">Browse</div>
                    </label>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                        <button class="btn btn-info btn-block mt-3" id="submitInfo" type="submit">Update</button>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-danger mt-3 btn-block" onclick="deleteAccount()" style="width: 195px" href="/deleteaccount?id=${updatedAccount.id}" role="button">Delete account</a>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-secondary mt-3 btn-block" style="width: 195px" href="/xml?id=${updatedAccount.id}" role="button">XML</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="/resources/js/phones.js"></script>
<%@include file="footer.jsp" %>