<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>
<script src="/resources/js/sockjs.min.js"></script>
<script src="/resources/js/stomp.min.js"></script>
<script src="/resources/js/chat.js"></script>
<div class="row grey lighten-5 no-gutters" style="min-height: 91vh;">
    <div class="col">
        <input name="sender" id="sender" value="${sessionScope.account.id}" hidden>
        <input name="receiver" id="receiver" value="${receiver.id}" hidden>
        <h5 class="mt-4 text-center" style="font-weight: 400">Chat with ${receiver.firstName} ${receiver.lastName}</h5>
        <div class="row justify-content-center">
            <div class="col-7 scrollbar scrollbar-pink bordered-pink thin" style="position: absolute; bottom: 0;">
                <c:forEach items="${conversations}" var="conversation">
                    <div class="row mt-3">
                        <div class="col-1 p-0 text-center">
                            <a href="/account?id=${conversation.sender.id}">
                                <img class="rounded-circle z-depth-1 center-block" src="/image?id=${conversation.sender.id}" width="60" height="60">
                            </a>
                        </div>
                        <div class="col-10 ml-4 border border-light rounded-lg">
                            <div class="row white">
                                <div class="col-12 font-weight-bolder mt-1 text-primary">
                                    <a href="/account?id=${conversation.sender.id}">${conversation.sender.firstName} ${conversation.sender.lastName}</a>
                                </div>
                                <div class="col-12 mt-1">${conversation.content}</div>
                                <div class="col-12 font-weight-light mt-1 mb-1 d-flex justify-content-end" style="font-size: 11px;">
                                    <fmt:parseDate value="${conversation.datetime}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both"/>
                                    <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
                <div id="lastMessage"></div>
                <div class="row mt-2 mb-4">
                    <div class="col mt-1 p-0">
                        <div class="form-group green-border-focus mb-1 mt-1">
                            <textarea class="form-control" name="text" id="text" rows="3" placeholder="Enter your message"></textarea>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" id="sendMessage" class="btn btn-primary btn-sm">send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>