<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>

<div class="row grey lighten-5 no-gutters" style="min-height: 91vh;">
    <div class="col">
        <h5 class="mt-4 text-center" style="font-weight: 400">All accounts</h5>
        <div class="row ml-auto mr-auto">
            <c:forEach items="${allAccounts}" var="account">
                <div class="col-3 mt-3 text-center">
                    <a href="/account?id=${account.id}"><h5>
                        <a href="/account?id=${account.id}"><div class="text-truncate pb-0">${account.firstName} ${account.lastName}</div></a></h5></a>
                    <a href="/account?id=${account.id}"><img class="rounded mt-1 mb-2 z-depth-1" src="/image?id=${account.id}" width="210" height="210"></a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>