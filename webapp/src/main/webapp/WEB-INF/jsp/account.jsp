<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters justify-content-between" style="min-height: 91vh;">
    <div class="col-3 mx-auto text-center">
        <h5 class="mt-3" style="font-weight: 400">${requestScope.account.firstName} ${requestScope.account.lastName}</h5>
        <img class="rounded mt-2 z-depth-1" src="/image?id=${requestScope.account.id}" width="210" height="210">
        <c:if test="${sessionScope.account.id != account.id}">
            <c:choose>
                <c:when test="${relationshipStatus.status == 1}">
                    <a class="btn btn-secondary mt-2 btn-block" style="width: 210px" href="/conversation/${account.id}" role="button"><em class="fas far fa-envelope-open pl-1 mr-2"></em>Send message</a>
                    <button type="button" class="btn btn-success mt-2 btn-block" style="width: 210px" disabled>
                        <em class="fas fa-user-friends pl-1 mr-2"></em>Already friend
                    </button>
                    <button id="modalActivate" type="button" class="btn btn-danger mt-2" style="width: 210px" data-toggle="modal" data-target="#exampleModalPreview">
                        <em class="fas fa-user-minus pl-1 mr-2"></em>Remove friend
                    </button>
                </c:when>
                <c:when test="${relationshipStatus.status == 0 && relationshipStatus.actionUser.id == sessionScope.account.id ||
            relationshipStatus.status == 2 && relationshipStatus.actionUser != sessionScope.account.id}">
                    <button type="button" class="btn btn-info mt-2 btn-block" style="width: 210px" disabled><em
                            class="fas fa-user-friends pl-1 mr-2"></em>Request is sent
                    </button>
                    <a class="btn btn-secondary mt-2 btn-block" style="width: 210px" href="/friend?delete=${requestScope.account.id}" role="button"><em class="fas fa-user-minus pl-1 mr-2"></em>Delete request</a>
                </c:when>
                <c:when test="${relationshipStatus.status == 0 && relationshipStatus.actionUser.id != sessionScope.account.id ||
            relationshipStatus.status == 2 && relationshipStatus.actionUser.id == sessionScope.account.id}">
                    <a class="btn btn-success mt-2 btn-block" style="width: 210px" href="/friend?approve=${requestScope.account.id}" role="button"><em class="fas fa-user-plus pl-1 mr-2"></em>Accept request</a>
                    <a class="btn btn-pink mt-2 btn-block" style="width: 210px" href="/friend?reject=${requestScope.account.id}" role="button"><em class="fas fa-minus-circle pl-1 mr-2"></em>Reject request</a>
                    <a class="btn btn-danger mt-2 btn-block" style="width: 210px" href="/friend?delete=${requestScope.account.id}" role="button"><em class="fas fa-user-minus pl-1 mr-2"></em>Remove request</a>
                </c:when>
                <c:otherwise>
                    <a class="btn btn-success mt-2 btn-block" style="width: 210px" href="/friend?sendrequest=${requestScope.account.id}" role="button"><em class="fas fa-user-friends pl-1 mr-2"></em>Add to friend</a>
                </c:otherwise>
            </c:choose>
        </c:if>
        <c:if test="${requestScope.account.id == sessionScope.account.id}">
            <a class="btn btn-info mt-2 btn-block" style="width: 210px" href="/updateaccount?id=${sessionScope.account.id}" role="button"><em class="fas fa-user-edit pl-1 mr-2"></em>Update account</a>
            <a class="btn btn-success mt-2 btn-block" style="width: 210px" href="/creategroup" role="button"><em class="fas fa-users pl-1 mr-2"></em>Create group</a>
        </c:if>
        <hr>
        <h5><a href="/friendlist?id=${requestScope.account.id}">Friends (${fn:length(friends)})</a></h5>
        <div class="row ml-3 mr-3 no-gutters">
            <c:forEach items="${requestScope.friends}" var="friend" begin="0" end="5">
                <div class="col-4">
                    <a href="/account?id=${friend.id}">
                        <img class="rounded-circle mt-2 mb-2 z-depth-1 center-block" src="/image?id=${friend.id}" width="60" height="60">
                    </a>
                    <div class="caption center-block">
                        <a href="/account?id=${friend.id}" style="font-size: 15px">${friend.firstName}</a></div>
                </div>
            </c:forEach>
        </div>
        <hr>
        <h5><a href="/grouplist?id=${requestScope.account.id}">Groups (${fn:length(groups)})</a></h5>
        <div class="row text-left ml-3 mb-3 justify-content-start">
            <c:forEach items="${requestScope.groups}" var="group" begin="0" end="2">
                <div class="col-3">
                    <a href="/group?id=${group.id}">
                        <img class="rounded mt-2 mb-2 z-depth-1 center-block" src="/imagegroup?groupid=${group.id}" width="60"
                             height="60"></a>
                </div>
                <div class="col-9 mt-1" style="word-wrap: break-word; width: 60px">
                    <a href="/group?id=${group.id}" style="font-size: 15px">${group.groupName}</a>
                    <div class="text-truncate pb-0" style="max-width: 150px">
                            ${group.groupDescription}
                    </div>
                </div>

            </c:forEach>
        </div>
    </div>
    <div class="col-8 mx-auto">
        <h5 class="mt-3 text-center" style="font-weight: 400">Information</h5>
        <div class="col-11 border border-light rounded-lg pt-2 pb-3 mt-3">
            <div class="col mt-2">First name: <span class="font-parameter">${requestScope.account.firstName}</span>
            </div>
            <div class="col mt-2">Last name: <span class="font-parameter">${requestScope.account.lastName}</span></div>
            <div class="col mt-2">Middle name: <span class="font-parameter">${requestScope.account.middleName}</span>
            </div>
            <div class="col mt-2">Date of birth: <span class="font-parameter">${requestScope.account.dob}</span></div>
            <c:forEach items="${requestScope.account.phone}" var="phones">
                <div class="col mt-2">
                        ${phones.type} phone: <span class="font-parameter">${phones.phone}</span>
                </div>
            </c:forEach>
            <div class="col mt-2">E-mail: <span class="font-parameter">${requestScope.account.email}</span></div>
            <div class="col mt-2">Home address: <span class="font-parameter">${requestScope.account.homeAddress}</span>
            </div>
            <div class="col mt-2">Work address: <span class="font-parameter">${requestScope.account.workAddress}</span>
            </div>
            <div class="col mt-2">ICQ: <span class="font-parameter">${requestScope.account.icq}</span></div>
            <div class="col mt-2">Skype: <span class="font-parameter">${requestScope.account.skype}</span></div>
            <div class="col mt-2">Additional information:
                <span class="font-parameter">${requestScope.account.additionalInformation}</span>
            </div>
            <div class="col mt-2">Registration Date:
                <span class="font-parameter">${requestScope.account.registrationDate}</span>
            </div>
        </div>
        <h5 class="mt-4 text-center" style="font-weight: 400">Wall message</h5>
        <div class="col-11 mt-1 p-0">
            <form action="/sendtoaccount" method="post">
                <div class="form-group green-border-focus mb-1 mt-1">
                    <input name="accountReceiver" value="${account.id}" hidden>
                    <textarea class="form-control" rows="3" placeholder="Enter your message" name="message"></textarea>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary btn-sm">send</button>
                </div>
            </form>
        </div>
        <div class="col-11 mt-2">
            <c:forEach items="${wallMessages}" var="wallMessage">
                <div class="row mt-2 border border-light rounded-lg white">
                    <div class="col-1 text-center">
                        <a href="/account?id=${wallMessage.sender.id}"><img class="rounded-circle z-depth-1 center-block mt-2" src="/image?id=${wallMessage.sender.id}" width="60" height="60"></a>
                    </div>
                    <div class="col-10 ml-3 ">
                        <div class="row">
                            <div class="col-12 font-weight-bolder mt-1 text-primary">
                                <a href="/account?id=${wallMessage.sender.id}">${wallMessage.sender.firstName} ${wallMessage.sender.lastName}</a>
                            </div>
                            <div class="col-12 mt-1">${wallMessage.message}</div>
                            <div class="col-10 font-weight-light mt-1 mb-1 d-flex justify-content-end" style="font-size: 11px;">${wallMessage.datesend}</div>
                            <c:if test="${account.id == sessionScope.account.id || wallMessage.sender.id == sessionScope.account.id}">
                            <div class="col-1 font-weight-light mt-1 mb-1 d-flex justify-content-end" style="font-size: 11px;"><a href="/deleteaccountmessage?id=${wallMessage.id}">Delete</a></div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<div class="modal fade right" id="exampleModalPreview" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalPreviewLabel">Remove friend</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Do you really want to remove friend?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                <a class="btn btn-success" href="/friend?delete=${requestScope.account.id}" role="button">Yes</a>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>