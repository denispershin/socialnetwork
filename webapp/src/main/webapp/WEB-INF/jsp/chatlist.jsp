<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>

<div class="row grey lighten-5 no-gutters" style="min-height: 91vh;">
    <div class="col">
        <h5 class="mt-4 text-center" style="font-weight: 400">Chat</h5>
        <div class="row justify-content-center">
            <div class="col-6">
                <c:forEach items="${chatList}" var="chat">
                <div class="row mt-2 border border-light rounded-lg white" style="height: 80px">
                    <div class="col-1 text-center"><img class="rounded-circle z-depth-1 center-block mt-2" src="/image?id=${chat.id}" width="60" height="60"></div>
                    <div class="col-9 ml-4">
                        <div class="row">
                            <div class="col-12 font-weight-bolder mt-1 text-primary"><a href=#>${chat.firstName} ${chat.lastName}</a></div>
                            <a href="/conversation/${chat.id}" class="stretched-link"></a>
                        </div>
                    </div>
                </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>