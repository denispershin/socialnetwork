<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="header.jsp" %>
<div class="row grey lighten-5 no-gutters justify-content-between" style="min-height: 91vh;">
    <div class="col-8 mx-auto">
        <h5 class="mt-4 text-center" style="font-weight: 400">Update "${requestScope.group.groupName}" group</h5>
        <div class="col-11 border border-light rounded-lg pt-2 pb-3 mt-3">
            <form action="/doUpdateGroup" method="post" enctype='multipart/form-data'>
                <input name="id" value="${group.id}" hidden>
                <input name="creator.id" value="${group.creator.id}" hidden>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-user"></em></span>
                    </div>
                    <input type="text" name="groupName" class="form-control" placeholder="Group name" value="${requestScope.group.groupName}" aria-label="Group name" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><em class="fas fa-info-circle"></em></span>
                    </div>
                    <textarea class="form-control rounded-0" name="groupDescription" id="exampleFormControlTextarea2" rows="3" placeholder="Group description" aria-label="Group description" aria-describedby="basic-addon1">${requestScope.group.groupDescription}</textarea>
                </div>
                <div class="input-default-wrapper mt-3">
                    <span class="input-group-text mb-3 border-right-0" style="border-bottom-right-radius: 0px; border-top-right-radius: 0px" id="input1"><em class="fas fa-camera-retro"></em></span>
                    <input type="file" name="avatar" id="file-with-current" class="input-default-js">
                    <label class="label-for-default-js rounded-right mb-3" for="file-with-current"><span class="span-choose-file">Choose group avatar</span>
                        <div class="float-right span-browse">Browse</div>
                    </label>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                        <button class="btn btn-info btn-block mt-3" type="submit">Update</button>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button id="modalActivate" type="button" class="btn btn-danger mt-2" data-toggle="modal" data-target="#exampleModalPreview">
                        <em class="fas fa-user-minus pl-1 mr-2"></em>Remove group
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade right" id="exampleModalPreview" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalPreviewLabel">Remove group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Do you really want to remove group?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                <a class="btn btn-success" href="/deletegroup?id=${requestScope.group.id}" role="button">Yes</a>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>