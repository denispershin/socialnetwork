<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Social Network</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/mdb.min.css" rel="stylesheet">
    <link href="/resources/css/style.css" rel="stylesheet">
    <style type="text/css">
        body, html {
            height: 100%;
        }
    </style>
</head>

<body class="mdb-color">
<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-5 flex-cols bg-white p-0 z-depth-1">
            <form class="text-center border border-light p-5" action="/login" method="post">
                <p class="h4 mb-4">Sign in</p>
                <c:if test="${not empty requestScope.loginerror}">
                    <div class="text-danger mb-2">${requestScope.loginerror}</div>
                </c:if>
                <input type="email" name="email" class="form-control mb-4" placeholder="E-mail" required>
                <input type="password" name="password" class="form-control mb-4" placeholder="Password" required>
                <div class="d-flex justify-content-around">
                    <div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="remember-me" class="custom-control-input" id="defaultLoginFormRemember">
                            <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                        </div>
                    </div>
                </div>
                <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>
                <p>Not a member? <a href="/registration">Register</a>
                </p>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery-3.4.0.js"></script>
<script type="text/javascript" src="/resources/js/popper.min.js"></script>
<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/js/mdb.js"></script>

</body>
</html>