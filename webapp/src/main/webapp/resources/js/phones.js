jQuery(function ($) {
    $(document.body).on('click', '#btn-remove-phone', function () {
        $(this).closest('#removePhoneHere').remove();
    });

    $('#formUpdate').submit(function () {
        var ok = confirm("Do you really want to update information?")
        if (ok === true) {
            return ok;
        } else {
            event.preventDefault();
        }
    });
});

var index = $('div #removePhoneHere').length ? 'undefined' : 0;

function func() {
    var regexp = /^(8|\+7)\d{10}$/;
    var phone = $('input[name=phone-no]').val();
    var type = $('#typeselect').val();
    var selected = null;
    if (type === "Home") {
        selected = '<option value="Home" selected>Home</option> <option value="Mobile">Mobile</option> <option value="Work">Work</option>'
    } else if (type === "Mobile") {
        selected = '<option value="Home" >Home</option> <option value="Mobile" selected>Mobile</option> <option value="Work">Work</option>'
    } else if (type === "Work") {
        selected = '<option value="Home">Home</option> <option value="Mobile">Mobile</option> <option value="Work" selected>Work</option>'
    }
    var valid = regexp.test(phone);
    if (valid) {
        $('#inputPhoneHere').append('' +
            '<div class="input-group phone-input no-gutters mt-3 mb-3" id="removePhoneHere">' +
            '<div class="col-2">' +
            '<select class="custom-select border-right" style="border-top-right-radius: 0; border-bottom-right-radius: 0" name="phone[' + index + '].type">' +
            selected +
            '</select>' +
            '</div>' +
            '<input type="text" name="phone[' + index + '].phone" class="form-control" value="' + phone + '" readonly>' +
            '<button type="button" class="btn-sm btn-danger px-3 border-0 border-left" style="border-top-left-radius: 0; border-bottom-left-radius: 0" id="btn-remove-phone"><i class="fas fa-times fa-lg"></i></button>' +
            '</div>'
        );
        index++;
        $('#remove-alert').remove();
        $('input[name=phone-no]').val('');
    } else {
        $('#remove-alert').remove();
        $('#inputPhoneAlert').append('' +
            '<div class="alert alert-danger" id="remove-alert" role="alert">Phone number incorrect. Phone should have mask +7xxxxxxx or 8xxxxxxx</div>'
        );
    }
}

function deleteAccount() {
    var userSelection = confirm("Do you really want to DELETE your account?");
    if (userSelection === true) {
        return userSelection;
    } else {
        event.preventDefault();
    }
}