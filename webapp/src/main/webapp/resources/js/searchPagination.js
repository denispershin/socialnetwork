jQuery(function ($) {
    $("#backacc").hide();
    $("#backgroup").hide();
    var pattern = $("#pattern").val();
    var accountsSize = $("#accountsSize").val();
    var groupSize = $("#groupSize").val();
    var recordsPerPage = 3;
    var currentPageAccount = 0;
    var currentPageGroup = 0;

    // search accounts

    $.ajax({
        url: '/searchAccount?search=' + pattern + '&page=' + currentPageAccount,
        method: "GET",
        dataType: "json",
        success: function (data) {
            if (accountsSize <= 3) {
                $("#nextacc").hide();
                $("#backacc").hide();
            }
            printAccounts(data);
        }
    });

    $("#nextacc").click(function () {
        $("#accountResult").html("");
        currentPageAccount++;
        accountsSize -= recordsPerPage;
        $("#backacc").show();
        $.ajax({
            url: '/searchAccount?search=' + pattern + '&page=' + currentPageAccount,
            method: "GET",
            dataType: "json",
            success: function (data) {
                if (accountsSize < 1) {
                    $("#nextacc").hide();
                    $("#accountResult").append("<div class=\"col text-center\" style='margin-top: 110px'>No more accounts found</div>");
                } else {
                    if (data.length <= recordsPerPage) {
                        $("#nextacc").hide();
                    }
                    printAccounts(data);
                }
            }
        });
    });

    $("#backacc").click(function () {
        $("#accountResult").html("");
        currentPageAccount--;
        accountsSize += recordsPerPage;
        $("#nextacc").show();
        if (currentPageAccount < 1) {
            $("#backacc").hide();
        }

        $.ajax({
            url: '/searchAccount?search=' + pattern + '&page=' + currentPageAccount,
            method: "GET",
            dataType: "json",
            success: function (data) {
                printAccounts(data);
            }
        });
    });

    function printAccounts(data) {
        for (var i = 0; i < recordsPerPage; ++i) {
            $("#accountResult").append(
                " <div class=\"col-3 mt-3 ml-5 text-center\">" +
                "<a href=\"/account?id=" + data[i].id + "\"><h5><div class=\"text-truncate pb-0\" style=\"max-width: 210px\">" + data[i].firstName + " " + data[i].lastName + "</div></h5></a>" +
                "<a href=\"/account?id=" + data[i].id + "\">" +
                "<img class=\"rounded mt-1 mb-2 z-depth-1\" src=\"/image?id=" + data[i].id + "\" width=\"210\" height=\"210\">" +
                "</a>" +
                "</div>")
        }
    }

    // search groups

    $.ajax({
        url: '/searchGroup?search=' + pattern + '&page=' + currentPageGroup,
        method: "GET",
        dataType: "json",
        success: function (data) {
            if (groupSize <= 3) {
                $("#nextgroup").hide();
                $("#backgroup").hide();
            }
            printGroup(data);
        }
    });

    $("#nextgroup").click(function () {
        $("#groupResult").html("");
        currentPageGroup++;
        groupSize -= recordsPerPage;
        $("#backgroup").show();
        $.ajax({
            url: '/searchGroup?search=' + pattern + '&page=' + currentPageGroup,
            method: "GET",
            dataType: "json",
            success: function (data) {
                if (groupSize < 1) {
                    $("#nextgroup").hide();
                    $("#groupResult").append("<div class=\"col text-center\" style='margin-top: 110px'>No more groups found</div>");
                } else {
                    if (data.length <= recordsPerPage) {
                        $("#nextgroup").hide();
                    }
                    printGroup(data);
                }
            }
        });
    });

    $("#backgroup").click(function () {
        $("#groupResult").html("");
        currentPageGroup--;
        groupSize += recordsPerPage;
        $("#nextgroup").show();
        if (currentPageGroup < 1) {
            $("#backgroup").hide();
        }

        $.ajax({
            url: '/searchGroup?search=' + pattern + '&page=' + currentPageGroup,
            method: "GET",
            dataType: "json",
            success: function (data) {
                printGroup(data);
            }
        });
    });

    function printGroup(data) {
        for (var i = 0; i < recordsPerPage; ++i) {
            $("#groupResult").append(
                " <div class=\"col-3 mt-3 ml-5 text-center\">" +
                "<a href=\"/group?id=" + data[i].id + "\"><h5><div class=\"text-truncate pb-0\" style=\"max-width: 210px\">" + data[i].groupName + "</div></h5></a>" +
                "<a href=\"/group?id=" + data[i].id + "\">" +
                "<img class=\"rounded mt-1 mb-2 z-depth-1\" src=\"/imagegroup?groupid=" + data[i].id + "\" width=\"210\" height=\"210\">" +
                "</a>" +
                "</div>")
        }
    }
});