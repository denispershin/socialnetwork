jQuery(function ($) {

    var stompClient = null;
    var sender = $('#sender').val();
    var receiver = $('#receiver').val();

    function getTopic() {
        if (sender < receiver) {
            return '/' + sender + '/' + receiver
        } else {
            return '/' + receiver + '/' + sender;
        }
    }

    function connect() {
        var socket = new SockJS('/chat');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic' + getTopic(), function (message) {
                addMessage(JSON.parse(message.body));
            });
        });
    }

    $('#sendMessage').click(function () {
        var text = $('#text');
        stompClient.send("/app/chat" + getTopic(), {},
            JSON.stringify({'text': text.val(), 'sender': sender, 'receiver': receiver}));
        text.val('')
    });

    function addMessage(message) {
        $('#lastMessage').before(
            "<div class=\"row mt-3\">" +
            "<div class=\"col-1 p-0 text-center\">" +
            "<a href=\"/account?id=" + message.sender +  "\">" +
            "<img class=\"rounded-circle z-depth-1 center-block\" src=\"/image?id=" + message.sender + "\" width=\"60\" height=\"60\">" +
            "</a>" +
            "</div>" +
            "<div class=\"col-10 ml-4 border border-light rounded-lg\">" +
            "<div class=\"row white\">" +
            "<div class=\"col-12 font-weight-bolder mt-1 text-primary\">" +
            "<a href=\"/account?id=" + message.sender + "\">" + message.firstName + " " + message.lastName + "</a>" +
            "</div>" +
            "<div class=\"col-12 mt-1\">" + message.text + "</div>" +
            "<div class=\"col-12 font-weight-light mt-1 mb-1 d-flex justify-content-end\" style=\"font-size: 11px;\">" + message.time +
            "</div></div></div></div>"
        );
    }

    connect();
});