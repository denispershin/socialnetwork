jQuery(function ($) {
    $('#ajaxsearch').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/search",
                data: {
                    filter: request.term
                },
                success: function (data) {
                    response($.map(data, function (object, i) {
                        return object.hasOwnProperty("firstName") ?
                            {
                                value: object.id,
                                type: "account",
                                label: object.firstName + ' ' + object.lastName
                            } :
                            {
                                value: object.id,
                                type: "group",
                                label: object.groupName
                            }
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            var id = ui.item.value;
            location.href = ui.item.type === "account" ? '/account?id=' + id : '/group?id=' + id;
            return false;
        }
    });
});