package com.getjavajob.training.socialnetwork.ui.controllers;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRelationship;
import com.getjavajob.training.socialnetwork.common.GroupRelationship;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.service.AccountRelationshipService;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupRelationshipService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class RelationshipController {

    private static final Logger logger = LoggerFactory.getLogger(RelationshipController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountRelationshipService accountRelationshipService;
    @Autowired
    private GroupRelationshipService groupRelationshipService;

    @GetMapping(value = "/friend", params = "sendrequest")
    public String sendFriendRequest(@RequestParam("sendrequest") int id, @SessionAttribute("account") Account accountFromSession) {
        AccountRelationship accountRelationship = new AccountRelationship();
        Account requestedAccount = accountService.getAccountById(id);
        setRelationship(accountRelationship, requestedAccount, accountFromSession);
        accountRelationship.setStatus(0);
        accountRelationship.setActionUser(accountFromSession);
        logger.info("Send request from account with id = {}, to id = {}", accountFromSession.getId(), id);
        accountRelationshipService.insert(accountRelationship);
        return "redirect:/account?id=" + requestedAccount.getId();
    }

    @GetMapping(value = "/friend", params = "approve")
    public String approveFriendRequest(@RequestParam("approve") int id, @SessionAttribute("account") Account accountFromSession) {
        AccountRelationship accountRelationship = new AccountRelationship();
        Account requestedAccount = accountService.getAccountById(id);
        setRelationship(accountRelationship, requestedAccount, accountFromSession);
        accountRelationship.setStatus(1);
        accountRelationship.setActionUser(accountFromSession);
        logger.info("Approve friendship between account with id = {}, and id = {}", accountFromSession.getId(), id);
        accountRelationshipService.update(accountRelationship);
        return "redirect:/account?id=" + requestedAccount.getId();
    }

    @GetMapping(value = "/friend", params = "reject")
    public String rejectFriendRequest(@RequestParam("reject") int id, @SessionAttribute("account") Account accountFromSession) {
        AccountRelationship accountRelationship = new AccountRelationship();
        Account requestedAccount = accountService.getAccountById(id);
        setRelationship(accountRelationship, requestedAccount, accountFromSession);
        accountRelationship.setStatus(2);
        accountRelationship.setActionUser(accountFromSession);
        logger.info("Reject friendship from account with id = {}, to id = {}", accountFromSession.getId(), id);
        accountRelationshipService.update(accountRelationship);
        return "redirect:/account?id=" + accountFromSession.getId();
    }

    @GetMapping(value = "/friend", params = "delete")
    public String deleteFriend(@RequestParam("delete") int id, @SessionAttribute("account") Account accountFromSession) {
        AccountRelationship accountRelationship = new AccountRelationship();
        Account requestedAccount = accountService.getAccountById(id);
        setRelationship(accountRelationship, requestedAccount, accountFromSession);
        logger.info("Delete friendship from account with id = {}, to id = {}", accountFromSession.getId(), id);
        accountRelationshipService.delete(accountRelationship);
        return "redirect:/account?id=" + accountFromSession.getId();
    }

    @GetMapping(value = "/grouprelationship", params = "sendrequest")
    public String sendGroupRequest(@RequestParam("sendrequest") int id, @SessionAttribute("account") Account accountFromSession) {
        GroupRelationship groupRelationship = new GroupRelationship();
        groupRelationship.setUser(accountFromSession);
        groupRelationship.setGroup(groupService.getGroupById(id));
        groupRelationship.setStatus(0);
        groupRelationship.setActionUser(accountFromSession);
        groupRelationship.setGroupRole(RoleType.USER);
        logger.info("Send request from account with id = {}, to group id = {}", accountFromSession.getId(), id);
        groupRelationshipService.insert(groupRelationship);
        return "redirect:/group?id=" + id;
    }

    @GetMapping(value = "/grouprelationship", params = {"approve", "groupid"})
    public String approveGroupRequest(
            @RequestParam("approve") int id,
            @RequestParam("groupid") int groupId,
            @SessionAttribute("account") Account accountFromSession
    ) {
        GroupRelationship groupRelationship = new GroupRelationship();
        groupRelationship.setUser(accountService.getAccountById(id));
        groupRelationship.setGroup(groupService.getGroupById(groupId));
        groupRelationship.setStatus(1);
        groupRelationship.setActionUser(accountFromSession);
        groupRelationship.setGroupRole(RoleType.USER);
        logger.info("Approve request for id = {}, to group id = {}, by id = {}", accountFromSession.getId(), id, groupRelationship.getGroup().getCreator().getId());
        groupRelationshipService.update(groupRelationship);
        return "redirect:/group?id=" + id;
    }

    @GetMapping(value = "/grouprelationship", params = "leave")
    public String leaveGroupRequest(@RequestParam("leave") int id, @SessionAttribute("account") Account accountFromSession) {
        GroupRelationship groupRelationship = new GroupRelationship();
        groupRelationship.setUser(accountFromSession);
        groupRelationship.setGroup(groupService.getGroupById(id));
        logger.info("Leave member with id = {}, from group id = {}", accountFromSession.getId(), id);
        groupRelationshipService.delete(groupRelationship);
        return "redirect:/group?id=" + id;
    }

    @GetMapping(value = "/grouprelationship", params = {"delete", "groupid"})
    public String deleteGroupRequest(
            @RequestParam("delete") int id,
            @RequestParam("groupid") int groupId
    ) {
        GroupRelationship groupRelationship = new GroupRelationship();
        groupRelationship.setUser(accountService.getAccountById(id));
        groupRelationship.setGroup(groupService.getGroupById(groupId));
        logger.info("Delete member with id = {}, from group id = {}", id, groupId);
        groupRelationshipService.delete(groupRelationship);
        return "redirect:/group?id=" + id;
    }

    @GetMapping(value = "/grouprelationship", params = {"addmoderator", "groupid"})
    public String addModerator(
            @RequestParam("addmoderator") int id,
            @RequestParam("groupid") int groupId,
            @SessionAttribute("account") Account accountFromSession
    ) {
        GroupRelationship accountAndGroupRelationship = groupRelationshipService.getGroupRelationshipStatus(accountFromSession.getId(), groupId);
        if (accountAndGroupRelationship.getGroupRole().equals(RoleType.ADMIN)) {
            GroupRelationship groupRelationship = new GroupRelationship();
            groupRelationship.setUser(accountService.getAccountById(id));
            groupRelationship.setGroup(groupService.getGroupById(groupId));
            groupRelationship.setGroupRole(RoleType.MODERATOR);
            logger.info("Add moderator with id = {}, to group id = {}", id, groupId);
            groupRelationshipService.update(groupRelationship);
        }
        return "redirect:/group?id=" + id;
    }

    @GetMapping(value = "/grouprelationship", params = {"deletemoderator", "groupid"})
    public String deleteModerator(
            @RequestParam("deletemoderator") int id,
            @RequestParam("groupid") int groupId,
            @SessionAttribute("account") Account accountFromSession
    ) {
        GroupRelationship accountAndGroupRelationship = groupRelationshipService.getGroupRelationshipStatus(accountFromSession.getId(), groupId);
        if (accountAndGroupRelationship.getGroupRole().equals(RoleType.ADMIN)) {
            GroupRelationship groupRelationship = new GroupRelationship();
            groupRelationship.setUser(accountService.getAccountById(id));
            groupRelationship.setGroup(groupService.getGroupById(groupId));
            groupRelationship.setGroupRole(RoleType.USER);
            logger.info("Delete moderator with id = {}, to group id = {}", id, groupId);
            groupRelationshipService.update(groupRelationship);
        }
        return "redirect:/group?id=" + id;
    }

    private void setRelationship(AccountRelationship relationship, Account requestedAccount, Account accountFromSession) {
        if (accountFromSession.getId() < requestedAccount.getId()) {
            relationship.setUserOne(accountFromSession);
            relationship.setUserTwo(requestedAccount);
        } else {
            relationship.setUserOne(requestedAccount);
            relationship.setUserTwo(accountFromSession);
        }
    }
}