package com.getjavajob.training.socialnetwork.ui.controllers;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.WallMessage;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import com.getjavajob.training.socialnetwork.service.WallMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.time.LocalDate;

@Controller
public class WallMessageController {

    private static final Logger logger = LoggerFactory.getLogger(WallMessageController.class);

    @Autowired
    private WallMessageService wallMessageService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @PostMapping("/sendtoaccount")
    public String sendToAccount(
            @RequestParam(name = "accountReceiver") int id,
            @RequestParam(name = "message") String message,
            @SessionAttribute("account") Account accountFromSession) {
        WallMessage wallMessage = new WallMessage();
        wallMessage.setAccountReceiver(accountService.getAccountById(id));
        wallMessage.setSender(accountFromSession);
        wallMessage.setMessage(message);
        wallMessage.setTypeMessage(WallMessage.TypeMessage.ACCOUNT);
        wallMessage.setDatesend(LocalDate.now());
        logger.info("Save wall message to account with id = {}", id);
        wallMessageService.save(wallMessage);
        return "redirect:/account?id=" + id;
    }

    @PostMapping("/sendtogroup")
    public String sendToGroup(
            @RequestParam(name = "groupReceiver") int id,
            @RequestParam(name = "message") String message,
            @SessionAttribute("account") Account accountFromSession) {
        WallMessage wallMessage = new WallMessage();
        wallMessage.setGroupReceiver(groupService.getGroupById(id));
        wallMessage.setSender(accountFromSession);
        wallMessage.setMessage(message);
        wallMessage.setTypeMessage(WallMessage.TypeMessage.GROUP);
        wallMessage.setDatesend(LocalDate.now());
        logger.info("Save wall message to group with id = {}", id);
        wallMessageService.save(wallMessage);
        return "redirect:/group?id=" + id;
    }

    @GetMapping("/deleteaccountmessage")
    public String deleteAccountMessage(@RequestParam("id") int id, @SessionAttribute("account") Account accountFromSession) {
        WallMessage wallMessage = wallMessageService.findById(id);
        if (wallMessage.getAccountReceiver().equals(accountFromSession) || wallMessage.getSender().equals(accountFromSession)) {
            logger.info("Delete wall message to account with id = {}", wallMessage.getAccountReceiver().getId());
            wallMessageService.delete(id);
        }
        return "redirect:/account?id=" + wallMessage.getAccountReceiver().getId();
    }

    @GetMapping("/deletegroupmessage")
    public String deleteGroupMessage(@RequestParam("id") int id, @SessionAttribute("account") Account accountFromSession) {
        WallMessage wallMessage = wallMessageService.findById(id);
        if (wallMessage.getGroupReceiver().getCreator().equals(accountFromSession) || wallMessage.getSender().equals(accountFromSession)) {
            logger.info("Delete wall message to group with id = {}", wallMessage.getGroupReceiver().getId());
            wallMessageService.delete(id);
        }
        return "redirect:/group?id=" + wallMessage.getGroupReceiver().getId();
    }
}