package com.getjavajob.training.socialnetwork.ui.controllers;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.ChatMessage;
import com.getjavajob.training.socialnetwork.common.ChatMessageDto;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.ChatMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class ChatMessageController {

    private static final Logger logger = LoggerFactory.getLogger(ChatMessageController.class);

    @Autowired
    private ChatMessageService chatMessageService;
    @Autowired
    private AccountService accountService;

    @GetMapping("/conversation/{receiver}")
    public ModelAndView conversationPage(@PathVariable("receiver") int receiverId, @SessionAttribute("account") Account accountFromSession) {
        logger.info("Start chat with account id = {}", receiverId);
        ModelAndView modelAndView = new ModelAndView("conversation");
        logger.info("Receive chat history id = {}, and id = {}", accountFromSession.getId(), receiverId);
        List<ChatMessage> allConversation = chatMessageService.getAllConversation(accountFromSession.getId(), receiverId);
        modelAndView.addObject("conversations", allConversation);
        modelAndView.addObject("receiver", accountService.getAccountById(receiverId));
        return modelAndView;
    }

    @MessageMapping("/chat/{sender}/{receiver}")
    @SendTo("/topic/{sender}/{receiver}")
    public ChatMessageDto chatMessage(ChatMessageDto message) {
        ChatMessage chatMessage = new ChatMessage();
        Account sender = accountService.getAccountById(message.getSender());
        Account receiver = accountService.getAccountById(message.getReceiver());
        chatMessage.setSender(sender);
        chatMessage.setReceiver(receiver);
        chatMessage.setContent(message.getText());
        chatMessage.setDatetime(LocalDateTime.now());
        logger.info("Save message, sender id = {}, receiver id = {}", sender.getId(), receiver.getId());
        chatMessageService.save(chatMessage);
        message.setTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")));
        message.setFirstName(sender.getFirstName());
        message.setLastName(sender.getLastName());
        return message;
    }

    @GetMapping("/chatlist")
    public ModelAndView chatListPage(@SessionAttribute("account") Account accountFromSession) {
        ModelAndView modelAndView = new ModelAndView("chatlist");
        logger.info("Receive all chats account id = {}", accountFromSession.getId());
        modelAndView.addObject("chatList", chatMessageService.getAllChats(accountFromSession.getId()));
        return modelAndView;
    }
}