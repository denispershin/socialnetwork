package com.getjavajob.training.socialnetwork.ui.controllers;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.Collections.emptyList;

@Controller
public class CommonController {

    private static final String DUMMY_IMAGE = "/resources/img/image.png";
    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
    private static final int RESULTS_ON_SEARCH_PAGE = 3;

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @GetMapping("/image")
    public ResponseEntity<byte[]> printAccountImage(@RequestParam(value = "id") int id) throws IOException {
        Account account = accountService.getAccountById(id);
        if (account.getAvatar() != null) {
            return ResponseEntity.ok().cacheControl(CacheControl.maxAge(864000, TimeUnit.SECONDS)).body(account.getAvatar());
        } else {
            return ResponseEntity.ok().cacheControl(CacheControl.maxAge(864000, TimeUnit.SECONDS)).body(Files.readAllBytes(Paths.get(DUMMY_IMAGE)));
        }
    }

    @GetMapping("/imagegroup")
    public void printGroupImage(@RequestParam("groupid") int groupId, HttpServletResponse response) throws IOException {
        Group group = groupService.getGroupById(groupId);
        if (group.getAvatar() != null) {
            response.getOutputStream().write(group.getAvatar());
        } else {
            response.sendRedirect(DUMMY_IMAGE);
        }
    }

    @PostMapping("/search")
    public ModelAndView search(@RequestParam(value = "search", required = false) String search) {
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("search", search);
        if (search == null || search.isEmpty()) {
            logger.info("Query for search request is blank");
            modelAndView.addObject("blankQuery", "Your search query is blank");
        } else {
            List<Account> accounts = accountService.searchAccounts(search);
            if (accounts.isEmpty()) {
                logger.info("Account for search request = {},  not found", search);
                modelAndView.addObject("accountsEmpty", "Accounts by your request not found");
            } else {
                modelAndView.addObject("accountsSize", accounts.size());
            }
            List<Group> groups = groupService.searchGroup(search);
            if (groups.isEmpty()) {
                logger.info("Account for search request = {}, not found", search);
                modelAndView.addObject("groupsEmpty", "Groups by your request not found");
            } else {
                modelAndView.addObject("groupSize", groups.size());
            }
        }
        return modelAndView;
    }

    @GetMapping("/searchAccount")
    @ResponseBody
    public List<Account> searchAccountPagination(@RequestParam("search") String search, @RequestParam("page") int page) {
        if (search.isEmpty()) {
            return emptyList();
        } else {
            logger.info("Account for search request = {}, page number = {}", search, page);
            List<Account> accounts = accountService.searchAccountsPagination(search, PageRequest.of(page, RESULTS_ON_SEARCH_PAGE));
            if (accounts.isEmpty()) {
                logger.info("Account for search request");
                return emptyList();
            } else {
                return accounts;
            }
        }
    }

    @GetMapping("/searchGroup")
    @ResponseBody
    public List<Group> searchGroupPagination(@RequestParam("search") String search, @RequestParam("page") int page) {
        if (search.isEmpty()) {
            return emptyList();
        } else {
            logger.info("Group for search request = {}, page number = {}", search, page);
            List<Group> groups = groupService.searchGroupPagination(search, PageRequest.of(page, RESULTS_ON_SEARCH_PAGE));
            if (groups.isEmpty()) {
                logger.info("Groups for search request not found");
                return emptyList();
            } else {
                return groups;
            }
        }
    }

    @GetMapping(value = "/search", params = "filter")
    @ResponseBody
    public List<Object> ajaxSearch(@RequestParam(value = "filter") String filter) {
        logger.info("Find account with {} word", filter);
        List<Account> accounts = accountService.searchAccounts(filter);
        logger.info("Find group with {} word", filter);
        List<Group> groups = groupService.searchGroup(filter);
        List<Object> result = new LinkedList<>();
        result.addAll(accounts);
        result.addAll(groups);
        return result;
    }
}