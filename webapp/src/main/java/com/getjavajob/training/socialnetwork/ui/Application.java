package com.getjavajob.training.socialnetwork.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@SpringBootApplication
@EnableJms
@ComponentScan("com.getjavajob.training.socialnetwork")
@EnableJpaRepositories("com.getjavajob.training.socialnetwork.dao")
@EntityScan("com.getjavajob.training.socialnetwork.common")
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

}