package com.getjavajob.training.socialnetwork.ui.controllers;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupRelationship;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.common.WallMessage;
import com.getjavajob.training.socialnetwork.service.GroupRelationshipService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import com.getjavajob.training.socialnetwork.service.WallMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

@Controller
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    private GroupRelationshipService groupRelationshipService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private WallMessageService wallMessageService;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    @GetMapping("/creategroup")
    public String registration() {
        return "creategroup";
    }

    @PostMapping("/doCreateGroup")
    public ModelAndView doRegistrationGroup(@ModelAttribute Group group, @SessionAttribute("account") Account accountFromSession) {
        if (group.getAvatar().length == 0) {
            group.setAvatar(null);
        }
        group.setCreator(accountFromSession);
        group.setRegistrationDate(LocalDate.now());
        logger.info("Create group with id = {} and name {}", group.getId(), group.getGroupName());
        groupService.create(group);
        return new ModelAndView("redirect:/group?id=" + group.getId());
    }

    @GetMapping("/updategroup")
    public ModelAndView updategroup(@RequestParam("id") int id, @SessionAttribute("account") Account accountFromSession) {
        ModelAndView modelAndView = new ModelAndView("updategroup");
        Group group = groupService.getGroupById(id);
        GroupRelationship groupRelationshipStatus = groupRelationshipService.getGroupRelationshipStatus(accountFromSession.getId(), group.getId());
        if (accountFromSession.getId() == group.getCreator().getId() ||
                groupRelationshipStatus.getGroupRole() == RoleType.ADMIN ||
                groupRelationshipStatus.getGroupRole() == RoleType.MODERATOR) {
            modelAndView.addObject("group", group);
            return modelAndView;
        } else {
            return new ModelAndView("redirect:/group?id=" + group.getId());
        }
    }

    @PostMapping("/doUpdateGroup")
    public ModelAndView doUpdateGroup(
            @RequestParam("id") int id,
            @ModelAttribute Group group,
            @SessionAttribute("account") Account accountFromSession
    ) {
        GroupRelationship groupRelationshipStatus = groupRelationshipService.getGroupRelationshipStatus(accountFromSession.getId(), id);
        if (accountFromSession.getId() == group.getCreator().getId() ||
                groupRelationshipStatus.getGroupRole() == RoleType.ADMIN ||
                groupRelationshipStatus.getGroupRole() == RoleType.MODERATOR) {
            if (group.getAvatar().length == 0) {
                group.setAvatar(groupService.getGroupById(group.getId()).getAvatar());
            }
            logger.info("Update group with id = {}", group.getId());
            groupService.update(group);
            return new ModelAndView("redirect:/group?id=" + group.getId());
        } else {
            ModelAndView modelAndView = new ModelAndView("group");
            modelAndView.addObject("group", group);
            modelAndView.addObject("updateerror", "You can edit only you own group");
            return modelAndView;
        }
    }

    @GetMapping("/group")
    public ModelAndView group(@RequestParam("id") int id, @SessionAttribute("account") Account accountFromSession) {
        ModelAndView modelAndView = new ModelAndView("group");
        logger.info("Get group by id = {}", id);
        Group group = groupService.getGroupById(id);
        if (group != null) {
            Account creatorAccount = group.getCreator();
            logger.info("Get member list for group with id = {}", group.getId());
            List<Account> memberList = groupRelationshipService.getGroupMembers(group);
            logger.info("Get status in group with id = {} ", group.getId());
            GroupRelationship groupRelationshipStatus = groupRelationshipService.getGroupRelationshipStatus(accountFromSession.getId(), group.getId());
            List<WallMessage> wallMessages = wallMessageService.findByGroupReceiver(group.getId());
            modelAndView.addObject("groupRelationshipStatus", groupRelationshipStatus);
            modelAndView.addObject("memberList", memberList);
            modelAndView.addObject("group", group);
            modelAndView.addObject("account", creatorAccount);
            modelAndView.addObject("wallMessages", wallMessages);
            return modelAndView;
        } else {
            return new ModelAndView("redirect:/login");
        }
    }

    @GetMapping("/memberlist")
    public ModelAndView memberListGroup(@RequestParam("id") int id, @SessionAttribute("account") Account accountFromSession) {
        ModelAndView modelAndView = new ModelAndView("memberlist");
        Group group = groupService.getGroupById(id);
        GroupRelationship groupRelationshipStatus = groupRelationshipService.getGroupRelationshipStatus(accountFromSession.getId(), group.getId());
        logger.info("Get member list in /memberlist for group with id = {}", group.getId());
        List<Account> memberList = groupRelationshipService.getGroupMembers(group);
        modelAndView.addObject("group", group);
        modelAndView.addObject("members", memberList);
        modelAndView.addObject("groupRelationshipStatus", groupRelationshipStatus);
        if (accountFromSession.getId() == group.getCreator().getId()) {
            List<Account> incomingMembersList = groupRelationshipService.getIncomingGroupMembers(group);
            modelAndView.addObject("incomingMembersList", incomingMembersList);
        }
        return modelAndView;
    }

    @GetMapping("/deletegroup")
    public String deleteGroup(@RequestParam("id") int id, @SessionAttribute("account") Account accountFromSession) {
        Group group = groupService.getGroupById(id);
        if (group != null) {
            if (accountFromSession.getId() == group.getCreator().getId()) {
                logger.info("Delete group with id = {}", group.getId());
                groupService.delete(group);
                return "redirect:account?id=" + accountFromSession.getId();
            } else {
                return "redirect:group?id=" + group.getId();
            }
        }
        return "redirect:account?id=" + accountFromSession.getId();
    }
}