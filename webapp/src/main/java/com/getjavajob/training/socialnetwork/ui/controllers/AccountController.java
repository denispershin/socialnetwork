package com.getjavajob.training.socialnetwork.ui.controllers;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRelationship;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.JmsNotificationMessage;
import com.getjavajob.training.socialnetwork.common.RoleType;
import com.getjavajob.training.socialnetwork.common.WallMessage;
import com.getjavajob.training.socialnetwork.service.AccountRelationshipService;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupRelationshipService;
import com.getjavajob.training.socialnetwork.service.JMSSender;
import com.getjavajob.training.socialnetwork.service.WallMessageService;
import com.getjavajob.training.socialnetwork.ui.config.SecurityUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;

@Controller
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private static final String SESSION_ACCOUNT_NAME = "account";
    private static final String REDIRECT_ACCOUNT_URL = "redirect:/account?id=";

    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountRelationshipService accountRelationshipService;
    @Autowired
    private GroupRelationshipService groupRelationshipService;
    @Autowired
    private WallMessageService wallMessageService;
    @Autowired
    private JMSSender jmsSender;
    @Autowired
    private JmsNotificationMessage notificationMessage;
    @Autowired
    private SecurityUtils securityUtils;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/doRegistration")
    public ModelAndView doRegistrationAccount(@ModelAttribute Account account, HttpSession session) {
        if (!accountService.accountIsExist(account.getEmail())) {
            if (account.getAvatar().length == 0) {
                account.setAvatar(null);
            }
            String password = account.getPassword();
            account.setRegistrationDate(LocalDate.now());
            accountService.create(account);
            logger.info("Registered account with id = {}, first name: {}, last name: {}", account.getId(), account.getFirstName(), account.getLastName());
            session.setAttribute(SESSION_ACCOUNT_NAME, account);
            securityUtils.autoLogin(account.getEmail(), password);
            logger.info("Send email to admins");
            notificationMessage.setMessage("New account with email: " + account.getEmail() + " is registered.");
            notificationMessage.setEmails(accountService.getAdminAccountEmails());
            jmsSender.sendMessage(notificationMessage);
            return new ModelAndView(REDIRECT_ACCOUNT_URL + account.getId());
        } else {
            ModelAndView modelAndView = new ModelAndView("registration");
            modelAndView.addObject("accountIsExist", "Account with this email is exist");
            return modelAndView;
        }
    }

    @GetMapping("/updateaccount")
    public String updateAccount(@RequestParam("id") int id, @AuthenticationPrincipal Account accountFromSession, Model model) {
        if (id == accountFromSession.getId() || accountFromSession.getAccountRole().equals(RoleType.ADMIN)) {
            model.addAttribute("updatedAccount", accountService.getAccountById(id));
            return "updateaccount";
        } else {
            return "redirect:/updateaccount?id=" + accountFromSession.getId();
        }
    }

    @PostMapping("/doUpdateAccount")
    public ModelAndView doUpdateAccount(
            @ModelAttribute Account account,
            @SessionAttribute(SESSION_ACCOUNT_NAME) Account accountFromSession,
            HttpSession session
    ) {
        ModelAndView modelAndView = new ModelAndView();
        Account accountBeforeUpdate = accountService.getAccountById(account.getId());
        String oldEmail = accountBeforeUpdate.getEmail();
        account.setId(accountBeforeUpdate.getId());
        if (account.getId() != accountFromSession.getId()) {
            account.setPassword(accountBeforeUpdate.getPassword());
        } else {
            account.setAccountRole(accountBeforeUpdate.getAccountRole());
        }
        if (account.getAvatar().length == 0) {
            account.setAvatar(accountBeforeUpdate.getAvatar());
        }
        if (account.getEmail().equals(oldEmail) || !accountService.accountIsExist(account.getEmail())) {
            if (account.getPhone() != null) {
                account.getPhone().removeIf(phone -> phone.getPhone() == null);
            }
            logger.info("Update account with id = {}", account.getId());
            accountService.update(account);
            if (account.getId() == accountFromSession.getId()) {
                session.setAttribute(SESSION_ACCOUNT_NAME, account);
                modelAndView.addObject(SESSION_ACCOUNT_NAME, account);
            }
            modelAndView.setViewName(REDIRECT_ACCOUNT_URL + account.getId());
            return modelAndView;
        } else if (accountService.accountIsExist(account.getEmail())) {
            logger.info("Tried to set exist email = {}, to account", account.getEmail());
            account.setEmail(oldEmail);
            modelAndView.addObject("emailIsExist", "Account with this email is exist");
            modelAndView.setViewName("updateaccount");
            return modelAndView;
        }
        return null;
    }

    @GetMapping("/login")
    public String login(@RequestParam(value = "error", required = false) String error, HttpSession session, Model model) {
        if (error != null) {
            model.addAttribute("loginerror", "Login or password incorrect");
            return "login";
        }
        Account accountFromSession = (Account) session.getAttribute(SESSION_ACCOUNT_NAME);
        if (accountFromSession != null) {
            return REDIRECT_ACCOUNT_URL + accountFromSession.getId();
        }
        return "login";
    }

    @GetMapping("/deleteaccount")
    public String deleteAccount(@RequestParam int id, HttpSession session) {
        Account account = (Account) session.getAttribute(SESSION_ACCOUNT_NAME);
        if (account.getId() == id || account.getAccountRole().equals(RoleType.ADMIN)) {
            logger.info("Delete account with id = {}", account.getId());
            accountService.deleteById(id);
            if (account.getId() == id) {
                return "redirect:/logout";
            }
        }
        return REDIRECT_ACCOUNT_URL + account.getId();
    }

    @GetMapping("/account")
    public ModelAndView accountPage(@RequestParam(value = "id") int id, @SessionAttribute(SESSION_ACCOUNT_NAME) Account accountFromSession) {
        ModelAndView modelAndView = new ModelAndView("account");
        logger.info("Open account with id = {}", id);
        Account account = accountService.getAccountById(id);
        if (account != null) {
            logger.info("Get friends for account with id = {}", account.getId());
            List<Account> friends = accountRelationshipService.getFriends(id);
            logger.info("Get groups for account with id = {}", account.getId());
            List<Group> groups = groupRelationshipService.getAccountGroup(id);
            logger.info("Get wall messages for account with id = {}", account.getId());
            List<WallMessage> wallMessages = wallMessageService.findByAccountReceiver(id);
            modelAndView.addObject("friends", friends);
            modelAndView.addObject("groups", groups);
            modelAndView.addObject("wallMessages", wallMessages);
            modelAndView.addObject(SESSION_ACCOUNT_NAME, account);
            if (id != accountFromSession.getId()) {
                AccountRelationship relationshipStatus;
                if (id < accountFromSession.getId()) {
                    relationshipStatus = accountRelationshipService.getRelationshipStatus(account.getId(), accountFromSession.getId());
                } else {
                    relationshipStatus = accountRelationshipService.getRelationshipStatus(accountFromSession.getId(), account.getId());
                }
                logger.info("Get friendship status for logged account id = {} and account id = {} ", accountFromSession.getId(), id);
                modelAndView.addObject("relationshipStatus", relationshipStatus);
            }
            return modelAndView;
        }
        return new ModelAndView("redirect:/login");
    }

    @GetMapping("/friendlist")
    public ModelAndView friendList(@RequestParam("id") int id, @SessionAttribute(SESSION_ACCOUNT_NAME) Account accountFromSession) {
        logger.info("Get friends for account with id = {}", id);
        List<Account> friendList = accountRelationshipService.getFriends(id);
        ModelAndView modelAndView = new ModelAndView("friendlist");
        modelAndView.addObject("friends", friendList);
        modelAndView.addObject("id", id);
        if (id == accountFromSession.getId()) {
            logger.info("Get incoming friend request for account id = {}", accountFromSession.getId());
            List<Account> incomingRequest = accountRelationshipService.getIncomingRequest(id);
            logger.info("Get outcoming friend request for account id = {}", accountFromSession.getId());
            List<Account> outcomingRequest = accountRelationshipService.getOutcomingRequest(id);
            modelAndView.addObject("incoming", incomingRequest);
            modelAndView.addObject("outcoming", outcomingRequest);
        }
        return modelAndView;
    }

    @GetMapping("/grouplist")
    public ModelAndView groupList(@RequestParam("id") int id, @SessionAttribute(SESSION_ACCOUNT_NAME) Account accountFromSession) {
        ModelAndView modelAndView = new ModelAndView("grouplist");
        logger.info("Get groups for account with id {}", id);
        List<Group> groups = groupRelationshipService.getAccountGroup(id);
        modelAndView.addObject("id", id);
        if (accountFromSession.getId() == id) {
            logger.info("Get outcoming groups request for account id = {}", accountFromSession.getId());
            List<Group> outcomingGroupRequest = groupRelationshipService.getAccountOutcomingGroups(id);
            modelAndView.addObject("outcoming", outcomingGroupRequest);
        }
        modelAndView.addObject("groups", groups);
        return modelAndView;
    }

    @GetMapping("/xml")
    public String xmlPage(@RequestParam("id") int id, @SessionAttribute(SESSION_ACCOUNT_NAME) Account accountFromSession) {
        if (accountFromSession.getId() == id) {
            return "xml";
        } else {
            return REDIRECT_ACCOUNT_URL + accountFromSession.getId();
        }
    }

    @PostMapping("/exportxml")
    public void exportXml(HttpServletResponse response, @SessionAttribute(SESSION_ACCOUNT_NAME) Account accountFromSession) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment; filename=account-" + accountFromSession.getId() + ".xml");
        XStream xStream = new XStream();
        xStream.processAnnotations(Account.class);
        logger.info("Save account to XML");
        String xml = xStream.toXML(accountFromSession);
        OutputStream outputStream = response.getOutputStream();
        outputStream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".getBytes());
        outputStream.write(xml.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
        response.sendRedirect("/xml?id=" + accountFromSession.getId());
    }

    @PostMapping("/xml")
    public String importXml(
            @RequestParam("xmlfile") MultipartFile file,
            @SessionAttribute(SESSION_ACCOUNT_NAME) Account accountFromSession,
            RedirectAttributes redirectAttributes
    ) throws IOException {
        XStream xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypes(new Class[]{Account.class});
        xStream.processAnnotations(Account.class);
        if (file.getSize() == 0) {
            logger.debug("File do not chosen");
            redirectAttributes.addFlashAttribute("notFile", "File do not chosen");
            return "redirect:/xml?id=" + accountFromSession.getId();
        } else {
            try {
                Account account = (Account) xStream.fromXML(file.getInputStream());
                if (account.getId() != accountFromSession.getId() && !accountFromSession.getAccountRole().equals(RoleType.ADMIN)) {
                    logger.debug("Tried to save account with another id");
                    redirectAttributes.addFlashAttribute("notSameId", "You tried save account with different id");
                    return "redirect:/xml?id=" + accountFromSession.getId();
                }
                logger.info("Save account from XML");
                redirectAttributes.addFlashAttribute("updatedAccount", account);
                return "redirect:/updateaccount?id=" + account.getId();
            } catch (XStreamException e) {
                logger.debug("Exception, error with xml file");
                redirectAttributes.addFlashAttribute("wrongXml", "You tried save account with wrong xml file");
                return "redirect:/xml?id=" + accountFromSession.getId();
            }
        }
    }

    @GetMapping("/allaccounts")
    public String allAccounts(Model model) {
        model.addAttribute("allAccounts", accountService.getAll());
        return "allaccounts";
    }
}