package com.getjavajob.training.socialnetwork.ui.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
public class ErrorPagesController implements ErrorController {

    @GetMapping("/error")
    public String errorPage(HttpServletResponse response) {
        if (response.getStatus() == HttpServletResponse.SC_FORBIDDEN) {
            return "/403";
        }
        return "/error";
    }

    @GetMapping("/403")
    public String forbiddenError() {
        return "403";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}