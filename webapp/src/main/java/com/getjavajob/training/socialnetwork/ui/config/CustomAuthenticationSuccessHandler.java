package com.getjavajob.training.socialnetwork.ui.config;

import com.getjavajob.training.socialnetwork.common.Account;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.socialnetwork.ui.config.SecurityUtils.getCurrentUser;

@Service
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        Account account = getCurrentUser();
        if (account != null) {
            httpServletRequest.getSession().setAttribute("account", account);
            httpServletResponse.sendRedirect("/account?id=" + account.getId());
        }
    }
}